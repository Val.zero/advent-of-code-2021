// Note : bad complexity, keeping this one for the record
pub fn solve1(input: &str) -> usize {
    let mut population = input
        .trim()
        .split(',')
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    let mut new_fishes = 0;

    for _ in 0..80 {
        // Each day, we look at each fish
        for fish in &mut population {
            // Does it produce a new fish ?
            if fish == &0 {
                new_fishes += 1;
                *fish = 7
            }
            *fish -= 1
        }
        // Adding the right number of new fishes to the population
        for _ in 0..new_fishes {
            population.push(8)
        }
        new_fishes = 0;
    }

    population.len()
}

/// Computes the size of a fish population after a given number of days.
///
/// # Arguments
/// * `initial_pop` - the initial fish population
/// * `days` - the number of days to grow the population
fn grow_pop(initial_pop: &[usize], days: usize) -> usize {
    // We are going to index the fish population by the number of days it will take them to produce
    // new ones
    let mut population = Vec::new();
    for _ in 0..9 {
        population.push(0)
    }
    for &fish in initial_pop {
        // Adding the initial population to the vec
        population[fish] += 1
    }

    // Each day, we decrement the number of each fish
    for _ in 0..days {
        // The number of fishes at 0 is the number of fishes we are going to add to the population
        let spawners = population[0];
        for i in 0..8 {
            population[i] = population[i + 1];
        }
        // Spawning fishes come back to 6
        population[6] += spawners;
        // Spawned fishes appear at 8
        population[8] = spawners
    }
    population.iter().sum()
}

pub fn solve2(input: &str) -> usize {
    let initial_population = input
        .trim()
        .split(',')
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    grow_pop(&initial_population, 256)
}

#[cfg(test)]
mod tests {
    use crate::days::day06::*;

    #[test]
    fn test_day06_solve1() {
        let test_data = "3,4,3,1,2";
        assert_eq!(solve1(test_data), 5934)
    }

    #[test]
    fn test_day06_solve2() {
        let test_data = "3,4,3,1,2";
        assert_eq!(solve2(test_data), 26984457539)
    }
}
