use std::collections::HashSet;

pub fn solve1(input: &str) -> usize {
    // Initializing variables
    let area = input
        .lines()
        .map(|l| l.chars().map(|c| c.to_digit(10).unwrap()).collect())
        .collect::<Vec<Vec<_>>>()
        .concat();
    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();
    let mut low_points = Vec::new();

    // Finding low points
    for y in 0..height {
        for x in 0..width {
            let n = area.get(x + y * width).unwrap();
            // Conditions take into account edge cases
            if (area
                .get(x + (y.checked_sub(1).unwrap_or(y + 1)) * width)
                .unwrap_or(&u32::MAX)
                > n)
                && (area
                    .get((x.checked_sub(1).unwrap_or(x + 1)) + y * width)
                    .unwrap_or(&u32::MAX)
                    > n)
                && (area.get((x + 1) + y * width).unwrap_or(&u32::MAX) > n)
                && (area.get(x + (y + 1) * width).unwrap_or(&u32::MAX) > n)
            {
                low_points.push(n)
            }
        }
    }

    // Summing up low points' risk level
    low_points.iter().fold(0, |acc, &n| acc + n + 1) as usize
}

/// Computes the size of a basin, defined by its low point, in a given area of fixed width and
/// height.
fn basin_size(low_point: (usize, usize), area: &[u32], width: usize, height: usize) -> i32 {
    let mut visit = HashSet::new();
    visit.insert(low_point);
    let mut visited = HashSet::new();
    let mut res = 0;

    // While there are cells to visit
    while !visit.is_empty() {
        // We prepare the sets of cells to visit after this iteration
        let mut visit_next = HashSet::new();
        for (x, y) in visit {
            // We have visited this cell, we count it once and only once
            res += 1;
            visited.insert((x, y));
            // upper cell
            if (y > 0)
                && (!visited.contains(&(x, y - 1)))
                && (*area.get(x + (y - 1) * width).unwrap() < 9)
            {
                visit_next.insert((x, y - 1));
            }
            // left cell
            if (x > 0)
                && (!visited.contains(&(x - 1, y)))
                && (*area.get((x - 1) + y * width).unwrap() < 9)
            {
                visit_next.insert((x - 1, y));
            }
            // right cell
            if (x < width - 1)
                && (!visited.contains(&(x + 1, y)))
                && (*area.get((x + 1) + y * width).unwrap() < 9)
            {
                visit_next.insert((x + 1, y));
            }
            // down cell
            if (y < height - 1)
                && (!visited.contains(&(x, y + 1)))
                && (*area.get(x + (y + 1) * width).unwrap() < 9)
            {
                visit_next.insert((x, y + 1));
            }
        }
        visit = visit_next
    }
    res
}

pub fn solve2(input: &str) -> usize {
    let area = input
        .lines()
        .map(|l| l.chars().map(|c| c.to_digit(10).unwrap()).collect())
        .collect::<Vec<Vec<_>>>()
        .concat();
    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();
    let mut low_points = Vec::new();

    // Find low points
    for y in 0..height {
        for x in 0..width {
            let n = area.get(x + y * width).unwrap();
            if (area
                .get(x + (y.checked_sub(1).unwrap_or(y + 1)) * width)
                .unwrap_or(&u32::MAX)
                > n)
                && (area
                    .get((x.checked_sub(1).unwrap_or(x + 1)) + y * width)
                    .unwrap_or(&u32::MAX)
                    > n)
                && (area.get((x + 1) + y * width).unwrap_or(&u32::MAX) > n)
                && (area.get(x + (y + 1) * width).unwrap_or(&u32::MAX) > n)
            {
                low_points.push((x, y))
            }
        }
    }

    // For each, find basin
    let mut basin_sizes = Vec::new();
    for point in low_points {
        basin_sizes.push(basin_size(point, &area, width, height))
    }

    // Take 3 largest
    basin_sizes.sort_unstable();
    basin_sizes
        .iter()
        .skip(basin_sizes.len() - 3)
        .product::<i32>() as usize
}

#[cfg(test)]
mod tests {
    use crate::days::day09::*;

    #[test]
    fn test_day09_solve1() {
        let test_data = "2199943210
3987894921
9856789892
8767896789
9899965678";
        assert_eq!(solve1(test_data), 15)
    }

    #[test]
    fn test_day09_solve2() {
        let test_data = "2199943210
3987894921
9856789892
8767896789
9899965678";
        assert_eq!(solve2(test_data), 1134)
    }
}
