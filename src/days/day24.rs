use rayon::prelude::{IntoParallelRefIterator, ParallelIterator};
use regex::Regex;
use std::cmp::Ordering;
use std::sync::Mutex;

#[derive(Debug)]
enum Instruction {
    Inp(String),
    Add(String, String),
    Mul(String, String),
    Div(String, String),
    Mod(String, String),
    Eql(String, String),
}

impl Instruction {
    fn new(name: &str, arg1: String, arg2: String) -> Self {
        match name {
            "inp" => Instruction::Inp(arg1),
            "add" => Instruction::Add(arg1, arg2),
            "mul" => Instruction::Mul(arg1, arg2),
            "div" => Instruction::Div(arg1, arg2),
            "mod" => Instruction::Mod(arg1, arg2),
            "eql" => Instruction::Eql(arg1, arg2),
            _ => panic!("Unknown instruction : {}", name),
        }
    }

    // fn execute(&self, mem: &mut HashMap<String, isize>, inp: &mut Iter<isize>) {
    //     match self {
    //         Instruction::Inp(arg) => {
    //             mem.insert(arg.to_string(), *inp.next().unwrap());
    //         }
    //         Instruction::Add(arg1, arg2) => {
    //             let a = *mem.get(arg1).unwrap();
    //             let b = match arg2.parse::<isize>() {
    //                 Ok(n) => n,
    //                 Err(_) => {
    //                     *mem.get(arg2).unwrap()
    //                 }
    //             };
    //             mem.insert(arg1.to_string(), a + b);
    //         }
    //         Instruction::Mul(arg1, arg2) => {
    //             let a = *mem.get(arg1).unwrap();
    //             // let b = arg2.parse::<isize>().unwrap_or(*mem.get(arg2).unwrap());
    //             let b = match arg2.parse::<isize>() {
    //                 Ok(n) => n,
    //                 Err(_) => {
    //                     *mem.get(arg2).unwrap()
    //                 }
    //             };
    //             mem.insert(arg1.to_string(), a * b);
    //         }
    //         Instruction::Div(arg1, arg2) => {
    //             let a = *mem.get(arg1).unwrap();
    //             let b = match arg2.parse::<isize>() {
    //                 Ok(n) => n,
    //                 Err(_) => {
    //                     *mem.get(arg2).unwrap()
    //                 }
    //             };
    //             mem.insert(arg1.to_string(), a / b);
    //         }
    //         Instruction::Mod(arg1, arg2) => {
    //             let a = *mem.get(arg1).unwrap();
    //             let b = match arg2.parse::<isize>() {
    //                 Ok(n) => n,
    //                 Err(_) => {
    //                     *mem.get(arg2).unwrap()
    //                 }
    //             };
    //             mem.insert(arg1.to_string(), a % b);
    //         }
    //         Instruction::Eql(arg1, arg2) => {
    //             let a = *mem.get(arg1).unwrap();
    //             let b = match arg2.parse::<isize>() {
    //                 Ok(n) => n,
    //                 Err(_) => {
    //                     *mem.get(arg2).unwrap()
    //                 }
    //             };
    //             mem.insert(arg1.to_string(), (a == b) as isize);
    //         }
    //     }
    // }

    fn execute_on_state(&self, state: &State) -> Vec<State> {
        let mut res = Vec::new();
        match self {
            Instruction::Inp(arg) => {
                for i in 1..10 {
                    let mut new_state = state.clone();
                    new_state.set(arg, i);
                    new_state.n.push(i as u8);
                    res.push(new_state)
                }
            }
            Instruction::Add(arg1, arg2) => {
                let a = state.get(arg1);
                let b = match arg2.parse::<i16>() {
                    Ok(n) => n,
                    Err(_) => state.get(arg2),
                };
                let mut new_state = state.clone();
                new_state.set(arg1, a + b);
                res.push(new_state)
            }
            Instruction::Mul(arg1, arg2) => {
                let a = state.get(arg1);
                let b = match arg2.parse::<i16>() {
                    Ok(n) => n,
                    Err(_) => state.get(arg2),
                };
                let mut new_state = state.clone();
                new_state.set(arg1, a * b);
                res.push(new_state)
            }
            Instruction::Div(arg1, arg2) => {
                let a = state.get(arg1);
                let b = match arg2.parse::<i16>() {
                    Ok(n) => n,
                    Err(_) => state.get(arg2),
                };
                let mut new_state = state.clone();
                new_state.set(arg1, a / b);
                res.push(new_state)
            }
            Instruction::Mod(arg1, arg2) => {
                let a = state.get(arg1);
                let b = match arg2.parse::<i16>() {
                    Ok(n) => n,
                    Err(_) => state.get(arg2),
                };
                let mut new_state = state.clone();
                new_state.set(arg1, a % b);
                res.push(new_state)
            }
            Instruction::Eql(arg1, arg2) => {
                let a = state.get(arg1);
                let b = match arg2.parse::<i16>() {
                    Ok(n) => n,
                    Err(_) => state.get(arg2),
                };
                let mut new_state = state.clone();
                new_state.set(arg1, (a == b) as i16);
                res.push(new_state)
            }
        }
        res
    }

    fn execute(&self, states: Vec<State>) -> Vec<State> {
        let acc = Mutex::new(Vec::new());
        states.par_iter().for_each(|state| {
            let add_res = self.execute_on_state(state);
            acc.lock().unwrap().push(add_res)
        });
        let mut res = acc.lock().unwrap().concat();
        // if let Instruction::Inp(_) = self {
        //
        //     // res.reverse();
        //     // res.dedup()
        // }
        res.sort_unstable_by(|s1, s2| {
            let w = s1.w.cmp(&s2.w);
            if w == Ordering::Equal {
                let x = s1.x.cmp(&s2.x);
                if x == Ordering::Equal {
                    let y = s1.y.cmp(&s2.y);
                    if y == Ordering::Equal {
                        let z = s1.z.cmp(&s2.z);
                        if z == Ordering::Equal {
                            // s1.get_n().cmp(&s2.get_n())
                            s1.n.last().unwrap().cmp(&s2.n.last().unwrap())
                        } else {
                            z
                        }
                    } else {
                        y
                    }
                } else {
                    x
                }
            } else {
                w
            }
        });
        res = my_dedup(res);
        res
        // for state in states {
        //     res.push(self.execute_on_state(state))
        // }
        // res.concat().into_iter().collect::<HashSet<_>>().into_iter().collect()
    }
}

fn my_dedup(v: Vec<State>) -> Vec<State> {
    let mut res = Vec::new();
    for w in v.windows(2) {
        let a = &w[0];
        let b = &w[1];
        if a != b {
            res.push(a.to_owned());
        }
    }
    let last = v.last().unwrap();
    if res.last().unwrap() != last {
        res.push(last.to_owned())
    }
    res
}

#[derive(Clone, Debug)]
struct State {
    w: i16,
    x: i16,
    y: i16,
    z: i16,
    n: Vec<u8>,
}

impl PartialEq for State {
    fn eq(&self, other: &Self) -> bool {
        self.w == other.w && self.x == other.x && self.y == other.y && self.z == other.z
    }
}

impl State {
    fn new(w: i16, x: i16, y: i16, z: i16) -> Self {
        State {
            w,
            x,
            y,
            z,
            n: Vec::with_capacity(14),
        }
    }

    fn get(&self, addr: &str) -> i16 {
        match addr {
            "w" => self.w,
            "x" => self.x,
            "y" => self.y,
            "z" => self.z,
            _ => panic!("Unrecognized address : {}", addr),
        }
    }

    fn set(&mut self, addr: &str, val: i16) {
        match addr {
            "w" => self.w = val,
            "x" => self.x = val,
            "y" => self.y = val,
            "z" => self.z = val,
            _ => panic!("Unrecognized address : {}", addr),
        }
    }

    fn get_n(&self) -> usize {
        let l = self.n.len() - 1;
        self.n
            .iter()
            .enumerate()
            .fold(0, |acc, (n, &i)| acc + i as usize * 10_usize.pow((l - n) as u32))
    }
}

// fn initialize_mem() -> HashMap<String, isize> {
//     let mut res = HashMap::new();
//     res.insert("w".to_string(), 0);
//     res.insert("x".to_string(), 0);
//     res.insert("y".to_string(), 0);
//     res.insert("z".to_string(), 0);
//     res
// }

pub fn solve1(input: &str) -> usize {
    let instr_re = Regex::new(r"([a-z]{3}) ([w-z])( .*)?").unwrap();
    let prog = input
        .lines()
        .map(|l| {
            let caps = instr_re.captures(l).unwrap();
            let name = &caps[1];
            let arg1 = caps[2].to_string();
            let arg2 = match caps.get(3) {
                None => "".to_string(),
                Some(m) => m.as_str().trim().to_string(),
            };
            Instruction::new(name, arg1, arg2)
        })
        .collect::<Vec<_>>();

    let mut states = vec![State::new(0, 0, 0, 0)];
    for (n, instr) in prog.iter().enumerate() {
        println!("instr {}, processing {} states", n, states.len());
        states = instr.execute(states);
    }
    // println!("Now {:?}", states);
    states
        .iter()
        .filter(|s| s.get(&"z".to_string()) == 0)
        .fold(0, |acc, s| {
            let n = s.get_n();
            if n > acc {
                n
            } else {
                acc
            }
        })

    // for i in &prog {
    //     println!("{:?}", i)
    // }

    // let mut ref_vec = [1;14];
    // let mut res = 0;
    // let mut res_val = 1;
    // while res_val != 0 {
    //     for i in 0..14 {
    //         let mut cur_vec = ref_vec;
    //         let mut min_n = 0;
    //         let mut min_val = isize::MAX;
    //         for n in 1..10 {
    //             cur_vec[13 - i] = n;
    //             let mut iter = cur_vec.iter();
    //             let mut mem = initialize_mem();
    //             for instr in &prog {
    //                 instr.execute(&mut mem, &mut iter)
    //             }
    //             let z_value = mem.get("z").unwrap();
    //             if z_value < &min_val {
    //                 min_val = *z_value;
    //                 min_n = n
    //             }
    //         }
    //         ref_vec[13 - i] = min_n;
    //         if i == 13 {
    //             res_val = min_val
    //         }
    //     }
    //     res = ref_vec.iter().map(|n|n.to_string()).collect::<String>().parse().unwrap();
    //     println!("{} : {}", res, res_val)
    // }

    // 'l: for n in 11111111111111_usize..=99999999999999 {
    //     let mut n_vec = Vec::with_capacity(14);
    //     for c in n.to_string().chars() {
    //         if c == '0' {
    //             continue 'l
    //         } else {
    //             n_vec.push(c.to_digit(10).unwrap() as isize)
    //         }
    //     }
    //     let mut iter = n_vec.iter();
    //     let mut mem = initialize_mem();
    //     for instr in &prog {
    //         instr.execute(&mut mem, &mut iter)
    //     }
    //     let z_value = mem.get("z").unwrap();
    //     if *z_value == 0 && n > max {
    //         max = n
    //     }
    //     println!("N : {}, Z : {}", n, z_value);
    // }
}

pub fn solve2(_input: &str) -> usize {
    unimplemented!();
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_day24_solve1() {
        unimplemented!()
    }

    #[test]
    fn test_day24_solve2() {
        unimplemented!()
    }
}
