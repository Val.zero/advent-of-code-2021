use regex::Regex;
use std::cmp::max;

/// Checks if (`x`, `y`) is in the square defined by \[`xmin`, `xmax`] and \[`ymin`, `ymax`]
fn is_in(x: isize, y: isize, xmin: isize, xmax: isize, ymin: isize, ymax: isize) -> bool {
    x >= xmin && x <= xmax && y >= ymin && y <= ymax
}

/// Computes one step (a new position and velocity) from given position and velocity
fn step(x: isize, y: isize, xvel: isize, yvel: isize) -> (isize, isize, isize, isize) {
    (
        x + xvel,
        y + yvel,
        (xvel.abs() - 1) * xvel.signum(),
        yvel - 1,
    )
}

/// Computes the result of a trajectory from an initial velocity and a target zone.
/// # Returns
/// `Some(m)` with `m` the maximum `y` position reached in the trajectory if the target is reached,
/// `None` otherwise.
fn trajectory(
    xvel: isize,
    yvel: isize,
    xmin: isize,
    xmax: isize,
    ymin: isize,
    ymax: isize,
) -> Option<isize> {
    let mut x = 0;
    let mut y = 0;
    let mut xvel = xvel;
    let mut yvel = yvel;
    let mut maxy = y;

    while y >= ymin && !is_in(x, y, xmin, xmax, ymin, ymax) {
        let (tmp_x, tmp_y, tmp_xvel, tmp_yvel) = step(x, y, xvel, yvel);
        x = tmp_x;
        y = tmp_y;
        xvel = tmp_xvel;
        yvel = tmp_yvel;
        maxy = max(y, maxy)
    }

    if is_in(x, y, xmin, xmax, ymin, ymax) {
        Some(maxy)
    } else {
        None
    }
}

pub fn solve1(input: &str) -> usize {
    let re = Regex::new(
        r"target area: x=(?P<xmin>-?\d+)..(?P<xmax>-?\d+), y=(?P<ymin>-?\d+)..(?P<ymax>-?\d+)",
    )
    .unwrap();
    let caps = re.captures(input.trim()).unwrap();
    let xmin = caps["xmin"].parse::<isize>().unwrap();
    let xmax = caps["xmax"].parse::<isize>().unwrap();
    let ymin = caps["ymin"].parse::<isize>().unwrap();
    let ymax = caps["ymax"].parse::<isize>().unwrap();
    let mut ymaxval = isize::MIN;

    for xvel in 0..xmax {
        for yvel in ymin..xmax {
            if let Some(max_traj) = trajectory(xvel, yvel, xmin, xmax, ymin, ymax) {
                ymaxval = max(max_traj, ymaxval)
            }
        }
    }
    ymaxval as usize
}

pub fn solve2(input: &str) -> usize {
    let re = Regex::new(
        r"target area: x=(?P<xmin>-?\d+)..(?P<xmax>-?\d+), y=(?P<ymin>-?\d+)..(?P<ymax>-?\d+)",
    )
    .unwrap();
    let caps = re.captures(input.trim()).unwrap();
    let xmin = caps["xmin"].parse::<isize>().unwrap();
    let xmax = caps["xmax"].parse::<isize>().unwrap();
    let ymin = caps["ymin"].parse::<isize>().unwrap();
    let ymax = caps["ymax"].parse::<isize>().unwrap();
    let mut res = 0;

    for xvel in 0..=xmax {
        for yvel in ymin..=xmax {
            if trajectory(xvel, yvel, xmin, xmax, ymin, ymax).is_some() {
                res += 1
            }
        }
    }
    res
}

#[cfg(test)]
mod tests {
    use crate::days::day17::*;

    #[test]
    fn test_day17_solve1() {
        let test_data = "target area: x=20..30, y=-10..-5";
        assert_eq!(solve1(test_data), 45)
    }

    #[test]
    fn test_day17_solve2() {
        let test_data = "target area: x=20..30, y=-10..-5";
        assert_eq!(solve2(test_data), 112)
    }
}
