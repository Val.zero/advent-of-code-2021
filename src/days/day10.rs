/// Computes the score of a corrupted line.
///
/// # Panics
/// This will panic if the line contains characters other than parenthesis or square, curly or
/// angular brackets.
fn corrupted_line_score(line: &str) -> usize {
    let mut acc = Vec::new();
    for c in line.chars() {
        // For every character in the line
        if matches!(c, '(' | '[' | '{' | '<') {
            // If it is an opening parenthesis/bracket, we push it into the stack
            acc.push(c)
        } else {
            // If not, it should be a closing one. If it doesn't match with the character at the top
            // of the stack, then we have found the source of the corruption
            let prev = acc.pop().unwrap();
            if !matches!((prev, c), ('(', ')') | ('[', ']') | ('{', '}') | ('<', '>')) {
                return match c {
                    ')' => 3,
                    ']' => 57,
                    '}' => 1197,
                    '>' => 25137,
                    _ => panic!("Unknown closing character : {}", c),
                };
            }
        }
    }
    0
}

pub fn solve1(input: &str) -> usize {
    let mut res = Vec::new();
    for line in input.lines() {
        res.push(corrupted_line_score(line))
    }
    res.iter().sum()
}

/// Computes the score of an incomplete line.
///
/// # Panics
/// This will panic if the line contains characters other than parenthesis or square, curly or
/// angular brackets.
fn incomplete_line_score(line: &str) -> usize {
    let mut acc = Vec::new();
    for c in line.chars() {
        if matches!(c, '(' | '[' | '{' | '<') {
            // If the char is an opening parenthesis/bracket, we push it into the stack
            acc.push(c)
        } else {
            // Else it should be a closing one (no verification though), so we pop the last one
            // We assume the line isn't corrupted, so closing characters should match opening ones
            acc.pop().unwrap();
        }
    }

    let mut res = 0;
    // We should go through opening parenthesis/brackets to determine which closing ones are
    // missing, so we must reverse the stack
    acc.reverse();
    for c in acc {
        res = res * 5
            + (match c {
                '(' => 1,
                '[' => 2,
                '{' => 3,
                '<' => 4,
                _ => panic!("Unknown opening character : {}", c),
            })
    }
    res
}

pub fn solve2(input: &str) -> usize {
    let mut res = Vec::new();
    for line in input.lines() {
        // We have to make sure incomplete lines aren't corrupted
        if corrupted_line_score(line) == 0 {
            res.push(incomplete_line_score(line))
        }
    }
    res.sort_unstable();
    *res.get(res.len() / 2).unwrap()
}

#[cfg(test)]
mod tests {
    use crate::days::day10::*;

    #[test]
    fn test_day10_solve1() {
        let test_data = "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";
        assert_eq!(solve1(test_data), 26397)
    }

    #[test]
    fn test_day10_solve2() {
        let test_data = "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";
        assert_eq!(solve2(test_data), 288957)
    }
}
