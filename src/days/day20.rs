/// Returns the value of a pixel represented by a character.
fn char_to_int(c: &char) -> usize {
    match c {
        '.' => 0,
        '#' => 1,
        _ => panic!("Unrecognized character : {}", c),
    }
}

/// Computes the value of a pixel at position (`x`, `y`) after applying the enhancement
/// algorithm (`algo`) on the image (`image_ref`) of size (`width` * `height`). Since the image is
/// supposed to have an infinite size, exterior pixels are treated as if their value was `void_char`.
fn new_pixel(
    x: usize,
    y: usize,
    width: usize,
    height: usize,
    image_ref: &[char],
    algo: &[char],
    void_char: usize,
) -> char {
    // Forming the binary representation of the result's index by checking every neighbor pixel
    let mut res_v = Vec::with_capacity(9);
    // Top line
    if y > 0 {
        if x > 0 {
            res_v.push(char_to_int(
                image_ref.get((x - 1) + (y - 1) * width).unwrap(),
            ));
        } else {
            res_v.push(void_char)
        }
        res_v.push(char_to_int(image_ref.get(x + (y - 1) * width).unwrap()));
        if x < width - 1 {
            res_v.push(char_to_int(
                image_ref.get((x + 1) + (y - 1) * width).unwrap(),
            ));
        } else {
            res_v.push(void_char)
        }
    } else {
        res_v.append(&mut vec![void_char; 3])
    }

    // Middle line
    if x > 0 {
        res_v.push(char_to_int(image_ref.get((x - 1) + y * width).unwrap()));
    } else {
        res_v.push(void_char)
    }
    res_v.push(char_to_int(image_ref.get(x + y * width).unwrap()));
    if x < width - 1 {
        res_v.push(char_to_int(image_ref.get((x + 1) + y * width).unwrap()));
    } else {
        res_v.push(void_char)
    }

    // Bottom line
    if y < height - 1 {
        if x > 0 {
            res_v.push(char_to_int(
                image_ref.get((x - 1) + (y + 1) * width).unwrap(),
            ));
        } else {
            res_v.push(void_char)
        }
        res_v.push(char_to_int(image_ref.get(x + (y + 1) * width).unwrap()));
        if x < width - 1 {
            res_v.push(char_to_int(
                image_ref.get((x + 1) + (y + 1) * width).unwrap(),
            ));
        } else {
            res_v.push(void_char)
        }
    } else {
        res_v.append(&mut vec![void_char; 3])
    }

    // Making the result's index
    let mut res_index = 0;
    for (n, &i) in res_v.iter().enumerate() {
        res_index += i << (8 - n)
    }

    *algo.get(res_index).unwrap()
}

pub fn solve1(input: &str) -> usize {
    let parts = input.split("\n\n").collect::<Vec<_>>();
    let algo = parts
        .get(0)
        .unwrap()
        .trim()
        .chars()
        .filter(|&c| c != '\n')
        .collect::<Vec<_>>();
    let mut height = parts.get(1).unwrap().lines().count();
    let mut width = parts.get(1).unwrap().lines().next().unwrap().len();
    let mut image = parts
        .get(1)
        .unwrap()
        .trim()
        .lines()
        .map(|l| l.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>()
        .concat();
    let pair_char = *algo.get(0).unwrap();
    let odd_char = if pair_char == '.' {
        '.'
    } else {
        *algo.get(8).unwrap()
    };

    // Applying two times the enhancement algorithm
    for i in 0..2 {
        // Adding a border (light or dark, depends on iteration parity and enhancement algorithm)
        let void_char = if i % 2 != 0 { pair_char } else { odd_char };
        let mut new_image_pre = vec![void_char; width + 2];
        for n in 0..height {
            new_image_pre.push(void_char);
            new_image_pre.append(&mut image[n * width..(n + 1) * width].to_vec());
            new_image_pre.push(void_char)
        }
        new_image_pre.append(&mut vec![void_char; width + 2]);
        height += 2;
        width += 2;

        // Applying algorithm
        let mut new_image = Vec::with_capacity(new_image_pre.len());
        for n in 0..new_image_pre.len() {
            let x = n % width;
            let y = n / width;
            new_image.push(new_pixel(
                x,
                y,
                width,
                height,
                &new_image_pre,
                &algo,
                char_to_int(&if i % 2 == 0 { odd_char } else { pair_char }),
            ))
        }
        image = new_image;
    }

    // Counting lit pixels
    image.iter().fold(0, |acc, &c| acc + (c == '#') as usize)
}

pub fn solve2(input: &str) -> usize {
    let parts = input.split("\n\n").collect::<Vec<_>>();
    let algo = parts
        .get(0)
        .unwrap()
        .trim()
        .chars()
        .filter(|&c| c != '\n')
        .collect::<Vec<_>>();
    let mut height = parts.get(1).unwrap().lines().count();
    let mut width = parts.get(1).unwrap().lines().next().unwrap().len();
    let mut image = parts
        .get(1)
        .unwrap()
        .trim()
        .lines()
        .map(|l| l.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>()
        .concat();
    let pair_char = *algo.get(0).unwrap();
    let odd_char = if pair_char == '.' {
        '.'
    } else {
        *algo.get(8).unwrap()
    };

    // Applying fifty times the enhancement algorithm
    for i in 0..50 {
        // Adding a border (light or dark, depends on iteration parity and enhancement algorithm)
        let void_char = if i % 2 != 0 { pair_char } else { odd_char };
        let mut new_image_pre = vec![void_char; width + 2];
        for n in 0..height {
            new_image_pre.push(void_char);
            new_image_pre.append(&mut image[n * width..(n + 1) * width].to_vec());
            new_image_pre.push(void_char)
        }
        new_image_pre.append(&mut vec![void_char; width + 2]);
        height += 2;
        width += 2;

        // Applying algorithm
        let mut new_image = Vec::with_capacity(new_image_pre.len());
        for n in 0..new_image_pre.len() {
            let x = n % width;
            let y = n / width;
            new_image.push(new_pixel(
                x,
                y,
                width,
                height,
                &new_image_pre,
                &algo,
                char_to_int(&if i % 2 == 0 { odd_char } else { pair_char }),
            ))
        }
        image = new_image;
    }

    // Counting lit pixels
    image.iter().fold(0, |acc, &c| acc + (c == '#') as usize)
}

#[cfg(test)]
mod tests {
    use crate::days::day20::*;

    #[test]
    fn test_day20_solve1() {
        let test_data = "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
#..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
.#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
.#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###";
        assert_eq!(solve1(test_data), 35)
    }

    #[test]
    fn test_day20_solve2() {
        let test_data = "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
#..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
.#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
.#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###";
        assert_eq!(solve2(test_data), 3351)
    }
}
