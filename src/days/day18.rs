// use crate::days::day18::NumElem::{Close, Comma, Num, Open};
//
// enum NumElem {
//     Open,
//     Close,
//     Num(u8),
//     Comma,
// }
//
// fn new_num(s: &str) -> Vec<Self> {
//     let mut res = Vec::new();
//     for c in s.chars() {
//         match c {
//             '[' => res.push(Open),
//             n if n.is_digit(10) => {
//                 res.push(Num(n.to_digit(10) as u8))
//             }
//             ']' => res.push(Close),
//             ',' => res.push(Comma),
//             _ => ()
//         }
//     }
//     res
// }
//
// fn explode(num: &Vec<NumElem>) -> bool {
//     let mut count_bracket = 0;
//     for (n, &elem) in num.iter().enumerate() {
//         match elem {
//             Open =>
//             Close => {}
//             Num(_) => {}
//             Comma => {}
//         }
//     }
//
//     false
// }

pub fn solve1(_input: &str) -> usize {
    unimplemented!();
}

pub fn solve2(_input: &str) -> usize {
    unimplemented!();
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_day18_solve1() {
        unimplemented!()
    }

    #[test]
    fn test_day18_solve2() {
        unimplemented!()
    }
}
