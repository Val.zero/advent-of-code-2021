use std::cmp::Ordering;
use std::sync::mpsc::channel;
use std::thread::spawn;

pub fn solve1(input: &str) -> usize {
    // Parsing binary integers
    let bin_ints: Vec<usize> = input
        .lines()
        .map(|l| usize::from_str_radix(l, 2).unwrap())
        .collect();
    // Getting the size of the input in lines)
    let list_size = bin_ints.len();
    // Getting the size of the integers (in bits)
    let data_size = input.lines().next().unwrap().trim().len();

    // Initializing an accumulator vector
    let mut acc = vec![0; data_size];
    for i in bin_ints {
        // For each binary integer, we look at each bit : is it 1 ?
        for (n, elem) in acc.iter_mut().enumerate().take(data_size) {
            if (i & (1 << n)) != 0 {
                *elem += 1; // Not sure about this one ...
            }
        }
    }

    let mut gamma_rate = 0;
    let mut epsilon_rate = 0;
    for (n, elem) in acc.iter().enumerate().take(data_size) {
        // Determining each bit's value (Note : epsilon = NOT(gamma))
        if (*elem) > (list_size / 2) {
            gamma_rate += 1 << n
        } else {
            epsilon_rate += 1 << n
        }
    }
    gamma_rate * epsilon_rate
}

pub fn solve2(input: &str) -> usize {
    // Parsing binary integers
    let mut bin_ints: Vec<usize> = input
        .lines()
        .map(|l| usize::from_str_radix(l, 2).unwrap())
        .collect();
    let mut bin_ints_cpy = bin_ints.clone();
    // Getting the size of the integers (in bits)
    let data_size = input.lines().next().unwrap().trim().len();

    // Computing the oxygen rating on a dedicated thread
    let (oxy_tx, oxy_rx) = channel();
    spawn(move || {
        for n in 1..=data_size {
            // For each bit of the input's integers
            let mut acc = 0.;
            for &i in &bin_ints {
                if (i & (1 << (data_size - n))) != 0 {
                    // If the nth bit is a 1
                    acc += 1.;
                }
            }
            // Value the bit should have
            let val = match acc
                .partial_cmp(&(f64::from(bin_ints.len() as i32) / 2.))
                .unwrap()
            {
                Ordering::Less => 0,
                Ordering::Equal => 1,
                Ordering::Greater => 1,
            };
            // Filtering the vector
            bin_ints = bin_ints
                .into_iter()
                .filter(|&b| (b & (1 << (data_size - n))) == (val << (data_size - n)))
                .collect();
            if bin_ints.len() == 1 {
                // Possible early end of loop
                break;
            }
        }
        oxy_tx.send(bin_ints.pop().unwrap_or_default()).unwrap()
    });

    // Computing the co2 rating on another thread
    let (co2_tx, co2_rx) = channel();
    spawn(move || {
        for n in 1..=data_size {
            // For each bit of the input's integers (now working on the copy of the initial vector)
            let mut acc = 0.;
            for &i in &bin_ints_cpy {
                if (i & (1 << (data_size - n))) != 0 {
                    // If the nth bit is a 1
                    acc += 1.;
                }
            }
            // Value the bit should have
            let val = match acc
                .partial_cmp(&(f64::from(bin_ints_cpy.len() as i32) / 2.))
                .unwrap()
            {
                Ordering::Less => 1,
                Ordering::Equal => 0,
                Ordering::Greater => 0,
            };
            // Filtering the vector
            bin_ints_cpy = bin_ints_cpy
                .into_iter()
                .filter(|&b| (b & (1 << (data_size - n))) == (val << (data_size - n)))
                .collect();
            if bin_ints_cpy.len() == 1 {
                // Possible early end of loop
                break;
            }
        }
        co2_tx.send(bin_ints_cpy.pop().unwrap_or_default()).unwrap()
    });

    // Receiving each thread's results
    let oxygen_rating = oxy_rx.recv().unwrap();
    let co2_rating = co2_rx.recv().unwrap();
    oxygen_rating * co2_rating
}

#[cfg(test)]
mod tests {
    use crate::days::day03::*;

    #[test]
    fn test_day03_solve1() {
        let test_data = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
        assert_eq!(solve1(test_data), 198)
    }

    #[test]
    fn test_day03_solve2() {
        let test_data = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
        assert_eq!(solve2(test_data), 230)
    }
}
