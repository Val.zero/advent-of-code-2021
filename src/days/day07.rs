use std::cmp::{max, min};

pub fn solve1(input: &str) -> usize {
    let mut list = input
        .trim()
        .split(',')
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    // The value we want the crabs to align is their median position, so we sort the vector ...
    list.sort_unstable();
    // ... and take the middle element
    let median = *list.get(list.len() / 2).unwrap();
    list.iter()
        .fold(0, |acc, &n| acc + max(median, n) - min(median, n))
}

pub fn solve2(input: &str) -> usize {
    let list = input
        .trim()
        .split(',')
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    // Now we want the mean positon
    let mean = (list.iter().sum::<usize>() as f64) / (list.len() as f64);
    // Taking into account floor and ceil to prevent rounding issues
    let mean_floor = mean.floor() as usize;
    let mean_ceil = mean.ceil() as usize;
    let res_floor = list.iter().fold(0, |acc, &n| {
        // The deviation is the difference between mean and actual position, its cost is defined as
        // the sum of the integers between 0 and itself, which is (n² + n)/2
        let deviation = max(mean_floor, n) - min(mean_floor, n);
        acc + (deviation * deviation + deviation) / 2
    });
    let res_ceil = list.iter().fold(0, |acc, &n| {
        let deviation = max(mean_ceil, n) - min(mean_ceil, n);
        acc + (deviation * deviation + deviation) / 2
    });
    min(res_floor, res_ceil)
}

#[cfg(test)]
mod tests {
    use crate::days::day07::*;

    #[test]
    fn test_day07_solve1() {
        let test_data = "16,1,2,0,4,2,7,1,2,14";
        assert_eq!(solve1(test_data), 37)
    }

    #[test]
    fn test_day07_solve2() {
        let test_data = "16,1,2,0,4,2,7,1,2,14";
        assert_eq!(solve2(test_data), 168)
    }
}
