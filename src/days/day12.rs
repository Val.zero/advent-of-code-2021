use rayon::prelude::{IntoParallelRefIterator, ParallelIterator};
use std::collections::hash_map::Entry::Vacant;
use std::collections::HashMap;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::Relaxed;

const START_ID: u8 = 0;
const END_ID: u8 = 1;

/// Computes all the possible paths leading to end from the current position, taking into account
/// the already visited caves.
///
/// # Arguments
/// * `graph` - the graph we are going through represented as a map linking a node to its neighbors
/// list
/// * `big_caves` - the list of big caves amongst the nodes
/// * `path` - the nodes already visited
/// * `node`  - the node we are currently on
///
/// # Returns
/// Returns the number of paths leading to end from the current position.
fn next_paths(
    graph: &HashMap<u8, Vec<u8>>,
    big_caves: &[u8],
    path: &mut Vec<u8>,
    node: u8,
) -> usize {
    let mut res = 0;
    for next_node in graph.get(&node).unwrap() {
        if *next_node == END_ID {
            res += 1
        } else if !path.contains(next_node) || big_caves.contains(next_node) {
            path.push(*next_node);
            res += next_paths(graph, big_caves, path, *next_node);
            path.pop();
        }
    }
    res
}

pub fn solve1(input: &str) -> usize {
    let mut str_to_id = HashMap::new();
    str_to_id.insert("start", START_ID);
    str_to_id.insert("end", END_ID);
    let mut big_caves = Vec::new();

    let mut graph: HashMap<_, Vec<_>> = HashMap::new();
    let mut counter = 1;
    for line in input.lines() {
        let nodes = line.split('-').collect::<Vec<&str>>();
        let fst = match str_to_id.get(nodes[0]) {
            None => {
                counter += 1;
                str_to_id.insert(nodes[0], counter);
                if nodes[0].chars().next().unwrap().is_uppercase() {
                    big_caves.push(counter)
                }
                counter
            }
            Some(&n) => n,
        };
        let snd = match str_to_id.get(nodes[1]) {
            None => {
                counter += 1;
                str_to_id.insert(nodes[1], counter);
                if nodes[1].chars().next().unwrap().is_uppercase() {
                    big_caves.push(counter)
                }
                counter
            }
            Some(&n) => n,
        };

        if let Vacant(e) = graph.entry(fst) {
            e.insert(vec![snd]);
        } else {
            graph.get_mut(&fst).unwrap().push(snd)
        }
        if let Vacant(e) = graph.entry(snd) {
            e.insert(vec![fst]);
        } else {
            graph.get_mut(&snd).unwrap().push(fst)
        }
    }

    next_paths(&graph, &big_caves, &mut vec![0], START_ID)
}

/// Computes all the possible paths leading to end from the current position, taking into account
/// the already visited caves. One small cave may be visited twice per path.
///
/// # Arguments
/// * `graph` - the graph we are going through represented as a map linking a node to its neighbors
/// list
/// * `big_caves` - the list of big caves amongst the nodes
/// * `path` - the nodes already visited
/// * `small_cave_twice` - true if we have already visited a small cave twice, false otherwise
/// * `node`  - the node we are currently on
///
/// # Returns
/// Returns the number of paths leading to end from the current position.
fn next_paths2(
    graph: &HashMap<u8, Vec<u8>>,
    big_caves: &[u8],
    path: Vec<u8>,
    small_cave_twice: bool,
    node: u8,
) -> usize {
    let res = AtomicUsize::new(0);
    // for next_node in graph.get(&node).unwrap() {
    //     if *next_node == END_ID {
    //         res += 1
    //     } else if !path.contains(next_node) || big_caves.contains(next_node) {
    //         path.push(*next_node);
    //         res += next_paths2(graph, big_caves, path, small_cave_twice, *next_node);
    //         path.pop();
    //     } else if !small_cave_twice && *next_node != 0 {
    //         path.push(*next_node);
    //         res += next_paths2(graph, big_caves, path, true, *next_node);
    //         path.pop();
    //     }
    // }
    graph.get(&node).unwrap().par_iter().for_each(|next_node| {
        if *next_node == END_ID {
            res.fetch_add(10, Relaxed);
        } else if !path.contains(next_node) || big_caves.contains(next_node) {
            let mut next_path = path.clone();
            next_path.push(*next_node);
            let score = next_paths2(graph, big_caves, next_path, small_cave_twice, *next_node);
            res.fetch_add(score, Relaxed);
        } else if !small_cave_twice && *next_node != 0 {
            let mut next_path = path.clone();
            next_path.push(*next_node);
            let score = next_paths2(graph, big_caves, next_path, true, *next_node);
            res.fetch_add(score, Relaxed);
        }
    });
    res.load(Relaxed)
}

pub fn solve2(input: &str) -> usize {
    let mut str_to_id = HashMap::new();
    str_to_id.insert("start", START_ID);
    str_to_id.insert("end", END_ID);
    let mut big_caves = Vec::new();

    let mut graph: HashMap<_, Vec<_>> = HashMap::new();
    let mut counter = 1;
    for line in input.lines() {
        let nodes = line.split('-').collect::<Vec<&str>>();
        let fst = match str_to_id.get(nodes[0]) {
            None => {
                counter += 1;
                str_to_id.insert(nodes[0], counter);
                if nodes[0].chars().next().unwrap().is_uppercase() {
                    big_caves.push(counter)
                }
                counter
            }
            Some(&n) => n,
        };
        let snd = match str_to_id.get(nodes[1]) {
            None => {
                counter += 1;
                str_to_id.insert(nodes[1], counter);
                if nodes[1].chars().next().unwrap().is_uppercase() {
                    big_caves.push(counter)
                }
                counter
            }
            Some(&n) => n,
        };

        if let Vacant(e) = graph.entry(fst) {
            e.insert(vec![snd]);
        } else {
            graph.get_mut(&fst).unwrap().push(snd)
        }
        if let Vacant(e) = graph.entry(snd) {
            e.insert(vec![fst]);
        } else {
            graph.get_mut(&snd).unwrap().push(fst)
        }
    }

    let mut path = Vec::with_capacity(graph.len());
    path.push(0);
    next_paths2(&graph, &big_caves, path, false, START_ID)
}

#[cfg(test)]
mod tests {
    use crate::days::day12::*;

    #[test]
    fn test_day12_solve1() {
        let test_data0 = "start-A
start-b
A-c
A-b
b-d
A-end
b-end";
        let test_data1 = "dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";
        let test_data2 = "fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW";
        assert_eq!(solve1(test_data0), 10);
        assert_eq!(solve1(test_data1), 19);
        assert_eq!(solve1(test_data2), 226)
    }

    #[test]
    fn test_day12_solve2() {
        let test_data0 = "start-A
start-b
A-c
A-b
b-d
A-end
b-end";
        let test_data1 = "dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";
        let test_data2 = "fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW";
        assert_eq!(solve2(test_data0), 36);
        assert_eq!(solve2(test_data1), 103);
        assert_eq!(solve2(test_data2), 3509)
    }
}
