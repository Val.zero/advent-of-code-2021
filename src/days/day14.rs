use std::collections::hash_map::Entry::Vacant;
use std::collections::HashMap;
use std::ops::AddAssign;

pub fn solve1(input: &str) -> usize {
    let parts = input.split("\n\n").collect::<Vec<_>>();

    // Getting the rules of polymerization from the input's second part
    let mut rules = HashMap::new();
    for line in parts.get(1).unwrap().lines() {
        let trans = line.split(" -> ").collect::<Vec<_>>();
        rules.insert(trans[0], trans[1].chars().next().unwrap());
    }

    // Getting the initial chain from the input's first line
    let mut chain = parts.get(0).unwrap().chars().collect::<Vec<char>>();
    for _ in 0..10 {
        let mut next_chain = vec![*chain.get(0).unwrap()];
        for w in chain.windows(2) {
            if let Some(&c) = rules.get(w.iter().collect::<String>().as_str()) {
                next_chain.push(c)
            }
            next_chain.push(*w.get(1).unwrap());
        }
        chain = next_chain
    }

    // Counting each character's occurrences
    let mut freqs = HashMap::new();
    for c in chain {
        if let Vacant(e) = freqs.entry(c) {
            e.insert(1);
        } else {
            freqs.get_mut(&c).unwrap().add_assign(1)
        }
    }
    freqs.values().max().unwrap() - freqs.values().min().unwrap()
}

// Will document this once it will be functioning
#[allow(dead_code)]
fn prod(a: u8, b: u8, n: u8, rules: &HashMap<(u8, u8), u8>, freqs: &mut HashMap<u8, usize>) {
    if n != 0 {
        if let Some(&c) = rules.get(&(a, b)) {
            prod(a, c, n - 1, rules, freqs);
            freqs.get_mut(&c).unwrap().add_assign(1);
            prod(c, b, n - 1, rules, freqs)
        } else {
            freqs.get_mut(&b).unwrap().add_assign(1)
        }
    }
}

pub fn solve2(input: &str) -> usize {
    let parts = input.split("\n\n").collect::<Vec<_>>();

    // Getting the rules of polymerization from the input's second part
    let mut rules = HashMap::new();
    for line in parts.get(1).unwrap().lines() {
        let trans = line.split(" -> ").collect::<Vec<_>>();
        let a = trans[0].chars().next().unwrap() as u8;
        let b = trans[0].chars().nth(1).unwrap() as u8;
        rules.insert((a, b), trans[1].chars().next().unwrap() as u8);
    }

    // Getting the initial chain from the input's first line
    let chain = parts.get(0).unwrap().chars().collect::<Vec<char>>();

    // Initializing the frequency map
    let mut freqs = HashMap::new();
    for &c in &chain {
        freqs.insert(c as u8, 0);
    }
    for &value in rules.values() {
        freqs.insert(value, 0);
    }
    freqs.insert(*chain.get(0).unwrap() as u8, 1);

    // Calculating frequencies
    // Note : not functionning for now !
    // for w in chain.windows(2) {
    //     prod(
    //         *w.get(0).unwrap() as u8,
    //         *w.get(1).unwrap() as u8,
    //         40,
    //         &rules,
    //         &mut freqs,
    //     );
    // }
    //
    // freqs.values().max().unwrap() - freqs.values().min().unwrap()
    usize::default()
}

#[cfg(test)]
mod tests {
    use crate::days::day14::*;

    #[test]
    fn test_day14_solve1() {
        let test_data = "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";
        assert_eq!(solve1(test_data), 1588)
    }

    #[test]
    fn test_day14_solve2() {
        let test_data = "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";
        assert_eq!(solve2(test_data), 2188189693529)
    }
}
