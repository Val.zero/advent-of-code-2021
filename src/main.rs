use crate::util::{get_input, parse_days, solver, time_formatting, timer};
use rayon::prelude::{IntoParallelRefIterator, ParallelIterator};
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use structopt::StructOpt;

mod days;
mod util;

/// Option parsing structure, see [structopt] crate documentation
///
/// [structopt]: https://docs.rs/structopt/0.3.25/structopt/
#[derive(StructOpt)]
struct Opt {
    /// Day of the month
    #[structopt(short, long, default_value = "1")]
    days: String,

    /// Solving the first puzzle of the day
    #[structopt(short, long)]
    first: bool,

    /// Solving the second puzzle of the day
    #[structopt(short, long)]
    second: bool,

    /// Enabling verbose mode
    #[structopt(short, long)]
    verbose: bool,

    /// Enabling timing
    #[structopt(short, long)]
    time: bool,
}

fn main() {
    // Parsing command line arguments
    let opt_arc = Arc::new(Opt::from_args());
    let opt = opt_arc.clone();
    let days = parse_days(&opt.days);

    if days.is_empty() {
        println!("Empty days list !")
    } else {
        // Making a map to store all results, so that we can display them in order at the end
        let res_map_mutex = Mutex::new(HashMap::new());

        // Computing each day's solution on a dedicated thread (more or less, see par_iter in Rayon
        // doc)
        days.par_iter().for_each(|&day| {
            let opt = opt_arc.clone();
            let input = get_input(day, opt.verbose);
            if opt.first {
                if opt.time {
                    let (res, time) = timer(solver(day, 0), &input);
                    res_map_mutex
                        .lock()
                        .unwrap()
                        .insert((day * 2) - 1, (res, time));
                } else {
                    let res = solver(day, 0)(&input);
                    res_map_mutex
                        .lock()
                        .unwrap()
                        .insert((day * 2) - 1, (res, u128::default()));
                }
            }
            if opt.second {
                if opt.time {
                    let (res, time) = timer(solver(day, 1), &input);
                    res_map_mutex.lock().unwrap().insert(day * 2, (res, time));
                } else {
                    let res = solver(day, 1)(&input);
                    res_map_mutex
                        .lock()
                        .unwrap()
                        .insert(day * 2, (res, u128::default()));
                }
            }
        });

        // Displaying results in the map in order
        let mut total_time = 0;
        let guard = res_map_mutex.lock().unwrap();
        for day in days {
            if opt.verbose {
                println!("Advent of Code day {}", day)
            }
            if opt.first {
                let (res, time) = guard.get(&((day * 2) - 1)).unwrap();
                println!("    Day {} part 1 solution : {}", day, res);
                if opt.time {
                    total_time += time;
                    println!("    Found in {}", time_formatting(*time))
                }
            }
            if opt.second {
                let (res, time) = guard.get(&(day * 2)).unwrap();
                println!("    Day {} part 2 solution : {}", day, res);
                if opt.time {
                    total_time += time;
                    println!("    Found in {}", time_formatting(*time))
                }
            }
        }
        if (opt.time) && (opt.verbose) {
            println!("Total computing time : {}", time_formatting(total_time))
        }
    }
}
