use crate::days::*;
use reqwest::{cookie::Jar, Url};
use std::fs;
use std::sync::Arc;
use std::time::Instant;

/// Parses the list of desired days to run.
///
/// # Arguments
/// * `days` - A `String` representing a list of days. Element of the list should be integers
/// between 1 and 25 (inclusive) or ranges of the form `n-m` with `n` and `m` integers between 1
/// and 25. Elements should be separated by commas (`,`). Spaces are tolerated.
///
/// # Returns
/// A sorted vector containing no duplicates.
///
/// # Panics
/// This will panic in the case of bad range formatting, that is having more than one hyphen per
/// range (eg. `1-3-6`).
pub fn parse_days(days: &str) -> Vec<usize> {
    let mut res = days
        .trim()
        .split(',') // Getting list elements
        .map(|range| {
            let bounds = range.trim().split('-').collect::<Vec<&str>>();
            match bounds.len() {
                0 => Vec::new(), // In case multiple commas are found, an empty element is produced
                1 => {
                    // Singleton
                    let n = bounds.get(0).unwrap().parse::<usize>().unwrap();
                    if n > 25 {
                        println!("Can't run days greater than 25 ({})", n);
                        Vec::new()
                    } else {
                        vec![n]
                    }
                }
                2 => {
                    // Range
                    let b1 = bounds.get(0).unwrap().parse::<usize>().unwrap();
                    let b2 = bounds.get(1).unwrap().parse::<usize>().unwrap();
                    let mut v = Vec::new();
                    for n in b1..=b2 {
                        if n > 25 {
                            println!("Can't run days greater than 25 ({})", n)
                        } else {
                            v.push(n)
                        }
                    }
                    v
                }
                _ => panic!("Can't parse range {}", range),
            }
        })
        .collect::<Vec<Vec<usize>>>()
        .concat();
    res.sort_unstable();
    res.dedup();
    res
}

/// Automatically get my input for the day.
///
/// # Panics
/// This will panic if `day` is larger than 25, or if the corresponding puzzle hasn't been released yet.
pub fn get_input(day: usize, verbose: bool) -> String {
    if verbose {
        println!("Getting input for day {} ...", day)
    }
    // First, we check the assets folder exists, and create it otherwise
    let assets_dir_path = std::path::Path::new("./assets");
    if !assets_dir_path.exists() {
        if verbose {
            println!("    Creating assets directory")
        }
        fs::create_dir(assets_dir_path).unwrap();
    }

    // Then we check if the desired input has already been downloaded
    let input_local_path_string = format!("./assets/input{}", day);
    let input_local_path = std::path::Path::new(&input_local_path_string);
    if input_local_path.exists() {
        // Then we open the file
        if verbose {
            println!("    [{}] From file", day)
        }
        fs::read_to_string(input_local_path).unwrap()
    } else {
        // Else we download it and return its content
        // Session ID is a unique token, inputs may vary
        // To get yours, follow https://github.com/wimglenn/advent-of-code-wim/issues/1
        let session_cookie = "session=53616c7465645f5fcf69dae605b3ed497652b42c3e9e3681ff604c8cbd1a4d30e8e0a9cd6042ff20a6ccec47f5f204d0";
        let url = format!("https://adventofcode.com/2021/day/{}/input", day)
            .parse::<Url>()
            .unwrap();
        let jar = Jar::default();
        jar.add_cookie_str(session_cookie, &url);
        let client = reqwest::blocking::ClientBuilder::new()
            .cookie_provider(Arc::new(jar))
            .build()
            .unwrap();
        let input_content = client.get(url).send().unwrap().text().unwrap();
        fs::write(input_local_path, input_content.as_bytes()).unwrap();
        if verbose {
            println!("    [{}] Downloaded input", day)
        }
        input_content
    }
}

/// Call the day's first puzzle solver.
///
/// # Panics
/// This will panic is `day` isn't between 1 and 25 (included).
fn solver_pt1(day: usize) -> fn(&str) -> usize {
    match day {
        1 => day01::solve1,
        2 => day02::solve1,
        3 => day03::solve1,
        4 => day04::solve1,
        5 => day05::solve1,
        6 => day06::solve1,
        7 => day07::solve1,
        8 => day08::solve1,
        9 => day09::solve1,
        10 => day10::solve1,
        11 => day11::solve1,
        12 => day12::solve1,
        13 => day13::solve1,
        14 => day14::solve1,
        15 => day15::solve1,
        16 => day16::solve1,
        17 => day17::solve1,
        18 => day18::solve1,
        19 => day19::solve1,
        20 => day20::solve1,
        21 => day21::solve1,
        22 => day22::solve1,
        23 => day23::solve1,
        24 => day24::solve1,
        25 => day25::solve1,
        _ => panic!("Day must be between 1 and 25"),
    }
}

/// Call the day's second puzzle solver.
///
/// # Panics
/// This will panic is `day` isn't between 1 and 25 (included).
fn solver_pt2(day: usize) -> fn(&str) -> usize {
    match day {
        1 => day01::solve2,
        2 => day02::solve2,
        3 => day03::solve2,
        4 => day04::solve2,
        5 => day05::solve2,
        6 => day06::solve2,
        7 => day07::solve2,
        8 => day08::solve2,
        9 => day09::solve2,
        10 => day10::solve2,
        11 => day11::solve2,
        12 => day12::solve2,
        13 => day13::solve2,
        14 => day14::solve2,
        15 => day15::solve2,
        16 => day16::solve2,
        17 => day17::solve2,
        18 => day18::solve2,
        19 => day19::solve2,
        20 => day20::solve2,
        21 => day21::solve2,
        22 => day22::solve2,
        23 => day23::solve2,
        24 => day24::solve2,
        25 => day25::solve2,
        _ => panic!("Day must be between 1 and 25"),
    }
}

/// Call the day's first or second puzzle solver.
///
/// # Panics
/// This will panic is `part` isn't 1 or 2.
pub fn solver(day: usize, part: usize) -> fn(&str) -> usize {
    match part {
        0 => solver_pt1(day),
        1 => solver_pt2(day),
        _ => panic!("Part must be 1 or 2"),
    }
}

/// Times the execution of a function with a given input.
///
/// # Returns
/// A pair consisting of the result of the function call and the measured duration of the execution.
pub fn timer(fun: fn(&str) -> usize, input: &str) -> (usize, u128) {
    let t = Instant::now();
    let res = fun(input);
    let dt = t.elapsed().as_nanos();
    (res, dt)
}

/// Formats a duration in nanoseconds.
pub fn time_formatting(duration: u128) -> String {
    match duration {
        n if n > 1_000_000_000 => format!("{:.3} s", ((duration as f64) / 1_000_000_000.)),
        n if n > 1_000_000 => format!("{:.3} ms", ((duration as f64) / 1_000_000.)),
        n if n > 1_000 => format!("{:.3} µs", ((duration as f64) / 1_000.)),
        n => format!("{} ns", n),
    }
}
