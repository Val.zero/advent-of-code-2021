pub fn solve1(input: &str) -> usize {
    // Defining mutable variables
    let mut h_pos = 0;
    let mut depth = 0;

    // Collecting lines and working on them
    let lines: Vec<&str> = input.lines().collect();
    for line in lines {
        // Each line should be "<instruction> <number>"
        let s: Vec<&str> = line.split(' ').collect();
        let num = s.get(1).unwrap().parse::<usize>().unwrap();
        // We modify our variables according to the <instruction> using the <number>
        match s.get(0).unwrap().to_string().as_str() {
            "forward" => h_pos += num,
            "up" => depth -= num,
            "down" => depth += num,
            _ => panic!("Unknown instruction"),
        }
    }

    // We return the product of ur variables
    h_pos * depth
}

pub fn solve2(input: &str) -> usize {
    // Same as before, but adding aim
    let mut h_pos = 0;
    let mut depth = 0;
    let mut aim = 0;

    let lines: Vec<&str> = input.lines().collect();
    for line in lines {
        let s: Vec<&str> = line.split(' ').collect();
        let num = s.get(1).unwrap().parse::<usize>().unwrap();
        // Core logic has changed
        match s.get(0).unwrap().to_string().as_str() {
            "forward" => {
                h_pos += num;
                depth += aim * num
            }
            "up" => aim -= num,
            "down" => aim += num,
            _ => panic!("Unknown instruction"),
        }
    }

    h_pos * depth
}

#[cfg(test)]
mod tests {
    use crate::days::day02::*;

    #[test]
    fn test_day02_solve1() {
        let test_data = "forward 5
down 5
forward 8
up 3
down 8
forward 2";
        assert_eq!(solve1(test_data), 150)
    }

    #[test]
    fn test_day02_solve2() {
        let test_data = "forward 5
down 5
forward 8
up 3
down 8
forward 2";
        assert_eq!(solve2(test_data), 900)
    }
}
