use std::collections::HashMap;

pub fn solve1(input: &str) -> usize {
    input
        .trim()
        .lines()
        .map(|l| {
            // For each line
            l.trim()
                .split('|') // We split it into two parts parts
                .nth(1)
                .unwrap() // Take only the second part
                .trim()
                .split(' ')
                .collect::<Vec<&str>>() // Collect its elements
        })
        .collect::<Vec<Vec<&str>>>()
        .concat() // Get all the lines' elements together
        .iter()
        .fold(0, |acc, digit| {
            if (digit.len() == 2) | (digit.len() == 3) | (digit.len() == 4) | (digit.len() == 7) {
                acc + 1 // And count only elements with a length of 2, 3, 4 or 7
            } else {
                acc
            }
        })
}

/// Checks if `s1` contains every character in `s2`.
fn contains_all(s1: &str, s2: &str) -> bool {
    for c in s2.chars() {
        if !s1.contains(c) {
            return false;
        }
    }
    true
}

/// Gets the associated with `digit` in `digit_map`. The twist is that characters in `digit` can be
/// in any order, so we must check that the character sets are equals, which means:
/// * their size is equal;
/// * every character in one is in the other.
///
/// # Panics
/// This will panic if no element of `digit_map` matches `digit`.
fn get_digit(digit: &str, digit_map: &HashMap<&str, usize>) -> usize {
    for d in digit_map.keys() {
        if (d.len() == digit.len()) && contains_all(d, digit) {
            return *digit_map.get(d).unwrap();
        }
    }
    panic!("Digit not found in map")
}

pub fn solve2(input: &str) -> usize {
    input
        .trim()
        .lines()
        .map(|l| {
            // For each line, we begin by splitting the two parts
            let parts = l
                .trim()
                .split('|')
                .map(|part| part.trim().split(' ').collect::<Vec<&str>>())
                .collect::<Vec<Vec<&str>>>();

            // We then map each digit in the first part with its real value
            let mut digit_map = HashMap::new();
            // 1, 4, 7 and 8 are immediate
            let digit1 = parts
                .get(0)
                .unwrap()
                .iter()
                .fold("", |acc, &s| if s.len() == 2 { s } else { acc });
            digit_map.insert(digit1, 1);
            let digit4 = parts
                .get(0)
                .unwrap()
                .iter()
                .fold("", |acc, &s| if s.len() == 4 { s } else { acc });
            digit_map.insert(digit4, 4);
            digit_map.insert(
                parts
                    .get(0)
                    .unwrap()
                    .iter()
                    .fold("", |acc, &s| if s.len() == 3 { s } else { acc }),
                7,
            );
            digit_map.insert(
                parts
                    .get(0)
                    .unwrap()
                    .iter()
                    .fold("", |acc, &s| if s.len() == 7 { s } else { acc }),
                8,
            );
            // 9 is the only digit of length 6 which contains every character of 4
            let digit9 = parts.get(0).unwrap().iter().fold("", |acc, &s| {
                if (s.len() == 6) && (contains_all(s, digit4)) {
                    s
                } else {
                    acc
                }
            });
            digit_map.insert(digit9, 9);
            // 0 is the only digit of length 6 which contains every character of 1 and is not 9
            let digit0 = parts.get(0).unwrap().iter().fold("", |acc, &s| {
                if (s.len() == 6) && (contains_all(s, digit1)) && (s != digit9) {
                    s
                } else {
                    acc
                }
            });
            digit_map.insert(digit0, 0);
            // 6 is the only digit of length 6 which is not 9 or 0
            digit_map.insert(
                parts.get(0).unwrap().iter().fold("", |acc, &s| {
                    if (s.len() == 6) && (s != digit9) && (s != digit0) {
                        s
                    } else {
                        acc
                    }
                }),
                6,
            );
            // 3 is the only digit of length 5 which contains every character of 1
            let digit3 = parts.get(0).unwrap().iter().fold("", |acc, &s| {
                if (s.len() == 5) && (contains_all(s, digit1)) {
                    s
                } else {
                    acc
                }
            });
            digit_map.insert(digit3, 3);
            // 5 is the only digit of length 5 which every character of is contained in 9 and is not 3
            let digit5 = parts.get(0).unwrap().iter().fold("", |acc, &s| {
                if (s.len() == 5) && (contains_all(digit9, s)) && (s != digit3) {
                    s
                } else {
                    acc
                }
            });
            digit_map.insert(digit5, 5);
            // 2 is the only digit of length 6 which is not 3 or 5
            digit_map.insert(
                parts.get(0).unwrap().iter().fold("", |acc, &s| {
                    if (s.len() == 5) && (s != digit3) && (s != digit5) {
                        s
                    } else {
                        acc
                    }
                }),
                2,
            );

            // Computing the value in the second part
            parts
                .get(1)
                .unwrap()
                .iter()
                .enumerate()
                .fold(0, |acc, (n, digit)| {
                    acc + get_digit(digit, &digit_map) * 10_usize.pow(3 - n as u32)
                })
        })
        .collect::<Vec<usize>>()
        .iter()
        .sum() // Summing the results of every line
}

#[cfg(test)]
mod tests {
    use crate::days::day08::*;

    #[test]
    fn test_day08_solve1() {
        let test_data =
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
        assert_eq!(solve1(test_data), 26)
    }

    #[test]
    fn test_day08_solve2() {
        let test_data =
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
        assert_eq!(solve2(test_data), 61229)
    }
}
