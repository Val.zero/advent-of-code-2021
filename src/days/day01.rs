/// Counting increasing elements in a list in place.
///
/// # Parameters
/// * `n` - previous element seen
/// * `l` - the list to look at
/// * `index` - index of the current element we're inspecting
fn count_increase(n: usize, l: &[usize], index: usize) -> usize {
    match l.get(index) {
        None => 0, // end of list
        Some(&m) => {
            if m > n {
                1 + count_increase(m, l, index + 1)
            } else {
                count_increase(m, l, index + 1)
            }
        }
    }
}

pub fn solve1(input: &str) -> usize {
    let data = input
        .lines()
        .map(|l| l.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    count_increase(usize::MAX, &data, 0)
}

/// Makes a list of triplets (sliding windows) from a list of integers.
fn make_windows_sum_list(l: &[usize]) -> Vec<usize> {
    let mut res = Vec::new();
    for i in 0..(l.len() - 2) {
        // Not using `get` here because I know my iterator is in bounds
        res.push((l[i], l[i + 1], l[i + 2]))
    }
    res.iter()
        .map(|(a, b, c)| a + b + c)
        .collect::<Vec<usize>>()
}

pub fn solve2(input: &str) -> usize {
    let data = input
        .lines()
        .map(|l| l.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    // Not entirely satisfied by having 2 different `map` on my lists, surely we could do this with 1
    count_increase(usize::MAX, &make_windows_sum_list(&data), 0)
}

#[cfg(test)]
mod tests {
    use crate::days::day01::*;

    #[test]
    fn test_day01_solve1() {
        let test_data = "199
200
208
210
200
207
240
269
260
263
";
        assert_eq!(solve1(test_data), 7)
    }

    #[test]
    fn test_day01_solve2() {
        let test_data = "199
200
208
210
200
207
240
269
260
263
";
        assert_eq!(solve2(test_data), 5)
    }
}
