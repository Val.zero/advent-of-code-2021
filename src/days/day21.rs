use std::cmp::max;
use std::ops::Range;

/// A deterministic dice, that is a dice that always give +1 of its previous result (or `start` if
/// the result was `stop`).
struct DeterministicDice<T> {
    iterator: Range<T>,
    start: T,
    stop: T,
}

impl DeterministicDice<usize> {
    /// Creates a deterministic dice from its bounds.
    fn new(start: usize, stop: usize) -> Self {
        DeterministicDice {
            iterator: start..stop + 1,
            start,
            stop,
        }
    }

    /// Gives the next number of the dice.
    fn next(&mut self) -> usize {
        match self.iterator.next() {
            None => {
                self.iterator = self.start..self.stop + 1;
                self.iterator.next().unwrap()
            }
            Some(n) => n,
        }
    }
}

pub fn solve1(input: &str) -> usize {
    let mut lines = input.lines();
    // Player 1 starting position and score
    let mut player1 = lines
        .next()
        .unwrap()
        .chars()
        .last()
        .unwrap()
        .to_digit(10)
        .unwrap() as usize;
    let mut score1 = 0;
    // Player 2 starting position and score
    let mut player2 = lines
        .next()
        .unwrap()
        .chars()
        .last()
        .unwrap()
        .to_digit(10)
        .unwrap() as usize;
    let mut score2 = 0;
    // Deterministic dice
    let mut dice = DeterministicDice::new(1, 100);
    let mut roll_count = 0;
    let mut turn = false;

    while score1 < 1000 && score2 < 1000 {
        turn = !turn;
        let roll1 = dice.next();
        let roll2 = dice.next();
        let roll3 = dice.next();
        roll_count += 3;

        // Is it player 1's turn ?
        if turn {
            player1 = (player1 + roll1 + roll2 + roll3) % 10;
            if player1 == 0 {
                score1 += 10
            } else {
                score1 += player1
            }
        } else {
            player2 = (player2 + roll1 + roll2 + roll3) % 10;
            if player2 == 0 {
                score2 += 10
            } else {
                score2 += player2
            }
        }
    }

    roll_count * (if turn { score2 } else { score1 })
}

/// Computes the number of universes where each player wins following a new roll.
///
/// # Arguments
/// * `position1` - player 1's current position
/// * `score1` - player 1's current score
/// * `position2` - player 2's current position
/// * `score2` - player 2's current score
/// * `turn1` - true if it's player 1's turn, false otherwise
/// * `cur_roll` - number of times the die has been rolled this turn
/// * `cur_val` - sum of the previous rolls this turn
fn roll(
    position1: usize,
    score1: usize,
    position2: usize,
    score2: usize,
    turn1: bool,
    cur_roll: usize,
    cur_val: usize,
) -> (usize, usize) {
    let (mut res1, mut res2) = (0, 0);
    if turn1 {
        // If it is player 1's turn
        if cur_roll == 3 {
            // If we've already rolled the dice 3 times
            let mut newpos = (position1 + cur_val) % 10;
            if newpos == 0 {
                newpos = 10
            }
            let newscore = score1 + newpos;
            if newscore > 20 {
                // If player 1 is winning this time
                res1 += 1
            } else {
                // Else it's player 2's turn
                let (add_res1, add_res2) = roll(newpos, newscore, position2, score2, !turn1, 0, 0);
                res1 += add_res1;
                res2 += add_res2
            }
        } else {
            // Else we roll the dice once again, and calculate the result in each universe
            for i in 1..4 {
                let (add_res1, add_res2) = roll(
                    position1,
                    score1,
                    position2,
                    score2,
                    turn1,
                    cur_roll + 1,
                    cur_val + i,
                );
                res1 += add_res1;
                res2 += add_res2
            }
        }
    } else {
        // If it is player 2's turn
        if cur_roll == 3 {
            // If we've already rolled the dice 3 times
            let mut newpos = (position2 + cur_val) % 10;
            if newpos == 0 {
                newpos = 10
            }
            let newscore = score2 + newpos;
            if newscore > 20 {
                // If player 2 is winning this time
                res2 += 1
            } else {
                // Else it's player 1's turn
                let (add_res1, add_res2) = roll(position1, score1, newpos, newscore, !turn1, 0, 0);
                res1 += add_res1;
                res2 += add_res2
            }
        } else {
            // Else we roll the dice once again, and calculate the result in each universe
            for i in 1..4 {
                let (add_res1, add_res2) = roll(
                    position1,
                    score1,
                    position2,
                    score2,
                    turn1,
                    cur_roll + 1,
                    cur_val + i,
                );
                res1 += add_res1;
                res2 += add_res2
            }
        }
    }
    // println!("pos1 : {}, score1 : {}, pos2 : {}, score2 : {}, res1 : {}, res2 : {}", position1, score1, position2, score2, res1, res2);
    (res1, res2)
}

pub fn solve2(input: &str) -> usize {
    let mut lines = input.lines();
    // Player 1 starting position
    let player1 = lines
        .next()
        .unwrap()
        .chars()
        .last()
        .unwrap()
        .to_digit(10)
        .unwrap() as usize;
    // Player 2 starting position
    let player2 = lines
        .next()
        .unwrap()
        .chars()
        .last()
        .unwrap()
        .to_digit(10)
        .unwrap() as usize;

    let (res1, res2) = roll(player1, 0, player2, 0, true, 0, 0);
    max(res1, res2)
}

#[cfg(test)]
mod tests {
    use crate::days::day21::*;

    #[test]
    fn test_day21_solve1() {
        let test_data = "Player 1 starting position: 4
Player 2 starting position: 8";
        assert_eq!(solve1(test_data), 739785)
    }

    #[test]
    fn test_day21_solve2() {
        let test_data = "Player 1 starting position: 4
Player 2 starting position: 8";
        assert_eq!(solve2(test_data), 444356092776315)
    }
}
