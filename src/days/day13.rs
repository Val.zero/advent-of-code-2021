use std::collections::HashSet;

pub fn solve1(input: &str) -> usize {
    // Splitting the input parts (points and folding instructions)
    let parts = input.split("\n\n").collect::<Vec<_>>();

    // Parsing all points into a set
    let mut points = HashSet::new();
    for line in parts[0].lines() {
        let coords = line
            .split(',')
            .map(|n| n.parse::<i16>().unwrap())
            .collect::<Vec<_>>();
        points.insert((coords[0], coords[1]));
    }

    // Parsing the first instruction
    let instruction = parts[1]
        .lines()
        .next()
        .unwrap()
        .split('=')
        .collect::<Vec<_>>();
    let (dir, val) = (
        instruction[0].chars().last().unwrap(),
        instruction[1].parse::<i16>().unwrap(),
    );
    points = points
        .iter()
        .map(|(x, y)| match dir {
            'x' => (val - (x - val).abs(), *y),
            'y' => (*x, val - (y - val).abs()),
            _ => panic!("Can only fold along x or y direction"),
        })
        .collect(); // Here the set is useful as it deletes duplicates after the fold
    points.len()
}

pub fn solve2(input: &str) -> usize {
    // Splitting the input parts (points and folding instructions)
    let parts = input.split("\n\n").collect::<Vec<_>>();

    // Parsing all points into a set
    let mut points = HashSet::new();
    for line in parts[0].lines() {
        let coords = line
            .split(',')
            .map(|n| n.parse::<i16>().unwrap())
            .collect::<Vec<_>>();
        points.insert((coords[0], coords[1]));
    }

    // Parsing instructions and applying them to the set of points
    for instruction_line in parts[1].lines() {
        let instruction = instruction_line.split('=').collect::<Vec<_>>();
        let (dir, val) = (
            instruction[0].chars().last().unwrap(),
            instruction[1].parse::<i16>().unwrap(),
        );
        points = points
            .iter()
            .map(|(x, y)| match dir {
                'x' => (val - (x - val).abs(), *y),
                'y' => (*x, val - (y - val).abs()),
                _ => panic!("Can only fold along x or y direction"),
            })
            .collect();
    }

    // In order to print the set, we need to approximate the width and height of the points grid
    let mut xmax = 0;
    let mut ymax = 0;
    for (x, y) in &points {
        if *x > xmax {
            xmax = *x
        }
        if *y > ymax {
            ymax = *y
        }
    }

    // Printing the set
    println!("Day 13 code: ");
    for y in 0..(ymax + 1) {
        for x in 0..(xmax + 1) {
            if points.contains(&(x, y)) {
                print!("#")
            } else {
                print!(" ")
            }
        }
        println!()
    }
    // Returning default until we can automatically parse the letters formed by the set
    usize::default()
}

#[cfg(test)]
mod tests {
    use crate::days::day13::*;

    #[test]
    fn test_day13_solve1() {
        let test_data = "6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5";
        assert_eq!(solve1(test_data), 17)
    }

    #[test]
    fn test_day13_solve2() {
        let test_data = "6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5";
        assert_eq!(solve2(test_data), 0)
    }
}
