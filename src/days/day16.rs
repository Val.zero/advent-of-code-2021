use std::ops::Not;
use std::vec::IntoIter;

/// Returns an iterator over the bits of an byte-sized unsigned integer.
fn bits_iter(i: u8) -> IntoIter<u8> {
    let mut v = Vec::with_capacity(8);
    for n in 0..8 {
        v.push((i & (128 >> n) == 0).not() as u8)
    }
    v.into_iter()
}

/// Returns an unsigned integer from a vector of bits (represented as bytes for lack of smaller
/// memory size).
fn bits_vec_to_int(v: Vec<u8>) -> usize {
    let l = v.len().checked_sub(1).unwrap_or_default();
    let mut res = 0;
    for (n, &i) in v.iter().enumerate() {
        res += (i as usize) << (l - n)
    }
    res
}

/// This structure is used to read an arbitrary number of bits from a given source as a stream.
struct BitStream {
    /// The source is stored as an iterator over byte-sized integers.
    source: IntoIter<u8>,
    /// The current chunk of the source is readable as an iterator over bits, represented as
    /// byte-sized integers.
    stream: IntoIter<u8>,
}

impl BitStream {
    /// Constructs a BitStream from a source (a vector of byte-sized integers).
    fn new(src: Vec<u8>) -> Self {
        BitStream {
            source: src.into_iter(),
            stream: Vec::new().into_iter(),
        }
    }

    /// Reads an arbitrary number of bits from the stream, returned as a vector of bits.
    fn take(&mut self, n: usize) -> Vec<u8> {
        let mut res = Vec::with_capacity(n);
        for _ in 0..n {
            if let Some(b) = self.stream.next() {
                // If the bit can be fetched from the current chunk of the stream, then we do it
                res.push(b)
            } else {
                // Else, we have to load another chunk
                if let Some(i) = self.source.next() {
                    // We split the chunk into bits ...
                    self.stream = bits_iter(i);
                    // ... and take the first one
                    res.push(self.stream.next().unwrap())
                }
            }
        }
        res
    }
}

/// Returns the sum of the stream's packets' version number.
fn sum_versions(stream: &mut BitStream) -> (usize, usize) {
    // Read version number
    let mut res = bits_vec_to_int(stream.take(3));
    let mut read = 3;
    // Read packet type
    let packet_type = bits_vec_to_int(stream.take(3));
    read += 3;

    if packet_type == 4 {
        // If the packet is a literal, we read the next 5 bits
        let mut chunk = stream.take(5);
        read += 5;

        // While first bit is 1, read again
        loop {
            // When first bit is 0, stop reading
            if *chunk.get(0).unwrap() == 0 {
                break;
            } else {
                chunk = stream.take(5);
                read += 5
            }
        }
    } else {
        // Read length type
        let length_type = bits_vec_to_int(stream.take(1));
        read += 1;

        if length_type == 0 {
            // Read length (15 bits number)
            let length = bits_vec_to_int(stream.take(15));
            read += 15;
            // Read next packet, add result, deduce length, then maybe read next packet
            let mut inner_read = 0;
            while inner_read < length {
                let (add_res, add_read) = sum_versions(stream);
                res += add_res;
                inner_read += add_read
            }
            read += inner_read
        } else {
            // Read length (11 bits number)
            let length = bits_vec_to_int(stream.take(11));
            read += 11;
            // Read n next packets, add result
            for _ in 0..length {
                let (add_res, add_read) = sum_versions(stream);
                res += add_res;
                read += add_read
            }
        }
    }
    (res, read)
}

pub fn solve1(input: &str) -> usize {
    let source = input
        .chars()
        .collect::<Vec<_>>()
        .windows(2)
        .step_by(2)
        .map(|cs| u8::from_str_radix(cs.iter().collect::<String>().as_str(), 16).unwrap())
        .collect::<Vec<_>>();
    let mut stream = BitStream::new(source);
    let (res, _) = sum_versions(&mut stream);
    res
}

/// Returns the value of an operation packet, according to its identifier and the values of its
/// sub-packets.
fn compute_operation(id: usize, values: &[usize]) -> usize {
    match id {
        0 => {
            // Sum
            values.iter().sum()
        }
        1 => {
            // Product
            values.iter().product()
        }
        2 => {
            // Minimum
            *values.iter().min().unwrap()
        }
        3 => {
            // Maximum
            *values.iter().max().unwrap()
        }
        5 => {
            // Greater than
            (values.get(0).unwrap() > values.get(1).unwrap()) as usize
        }
        6 => {
            // Lesser than
            (values.get(0).unwrap() < values.get(1).unwrap()) as usize
        }
        7 => {
            // Equal to
            (values.get(0).unwrap() == values.get(1).unwrap()) as usize
        }
        _ => panic!("Unknown operation identifier : {}", id),
    }
}

/// Returns the value of the outermost packet.
fn compute_transmission(stream: &mut BitStream) -> (usize, usize) {
    // Read version number
    let _ = bits_vec_to_int(stream.take(3));
    let mut read = 3;
    // Read packet type
    let packet_type = bits_vec_to_int(stream.take(3));
    read += 3;
    let res;

    if packet_type == 4 {
        // If the packet is a literal, we read the next 5 bits
        let mut chunk = stream.take(5);
        read += 5;

        let mut tmp = Vec::new();
        // While first bit is 1, read again
        loop {
            tmp.push(chunk[1..].to_vec());
            // When first bit is 0, stop reading
            if *chunk.get(0).unwrap() == 0 {
                break;
            } else {
                chunk = stream.take(5);
                read += 5
            }
        }
        res = bits_vec_to_int(tmp.concat());
    } else {
        // Read length type
        let length_type = bits_vec_to_int(stream.take(1));
        read += 1;
        let mut inner_values = Vec::new();

        if length_type == 0 {
            // Read length (15 bits number)
            let length = bits_vec_to_int(stream.take(15));
            read += 15;
            // Read next packet, add result, deduce length, then maybe read next packet
            let mut inner_read = 0;
            while inner_read < length {
                let (val, add_read) = compute_transmission(stream);
                inner_values.push(val);
                inner_read += add_read
            }
            read += inner_read
        } else {
            // Read length (11 bits number)
            let length = bits_vec_to_int(stream.take(11));
            read += 11;
            // Read n next packets, add result
            for _ in 0..length {
                let (val, add_read) = compute_transmission(stream);
                inner_values.push(val);
                read += add_read;
            }
        }
        res = compute_operation(packet_type, &inner_values);
    }
    (res, read)
}

pub fn solve2(input: &str) -> usize {
    let source = input
        .chars()
        .collect::<Vec<_>>()
        .windows(2)
        .step_by(2)
        .map(|cs| u8::from_str_radix(cs.iter().collect::<String>().as_str(), 16).unwrap())
        .collect::<Vec<_>>();
    let mut stream = BitStream::new(source);
    let (res, _) = compute_transmission(&mut stream);
    res
}

#[cfg(test)]
mod tests {
    use crate::days::day16::*;

    #[test]
    fn test_day16_solve1() {
        let test_data1 = "8A004A801A8002F478";
        let test_data2 = "620080001611562C8802118E34";
        let test_data3 = "C0015000016115A2E0802F182340";
        let test_data4 = "A0016C880162017C3686B18A3D4780";
        assert_eq!(solve1(test_data1), 16);
        assert_eq!(solve1(test_data2), 12);
        assert_eq!(solve1(test_data3), 23);
        assert_eq!(solve1(test_data4), 31);
    }

    #[test]
    fn test_day16_solve2() {
        let test_data1 = "C200B40A82";
        let test_data2 = "04005AC33890";
        let test_data3 = "880086C3E88112";
        let test_data4 = "CE00C43D881120";
        let test_data5 = "D8005AC2A8F0";
        let test_data6 = "F600BC2D8F";
        let test_data7 = "9C005AC2F8F0";
        let test_data8 = "9C0141080250320F1802104A08";
        assert_eq!(solve2(test_data1), 3);
        assert_eq!(solve2(test_data2), 54);
        assert_eq!(solve2(test_data3), 7);
        assert_eq!(solve2(test_data4), 9);
        assert_eq!(solve2(test_data5), 1);
        assert_eq!(solve2(test_data6), 0);
        assert_eq!(solve2(test_data7), 0);
        assert_eq!(solve2(test_data8), 1);
    }
}
