use std::cmp::{max, min, Ordering};

pub fn solve1(input: &str) -> usize {
    // List of coordinates quadruplets
    let mut coords_list = Vec::new();
    // Height and width of the grid we're going to work on
    let mut height = 0;
    let mut width = 0;
    for line in input.lines() {
        // Splintting the line
        let coords = line
            .trim()
            .split(" -> ")
            .map(|pair| pair.trim().split(',').collect::<Vec<&str>>())
            .collect::<Vec<Vec<&str>>>()
            .concat();
        // Parsing numbers only if relevant for now, that is if the line is horizontal or vertical
        if (coords[0] == coords[2]) || (coords[1] == coords[3]) {
            let x1 = coords[0].parse::<usize>().unwrap();
            let y1 = coords[1].parse::<usize>().unwrap();
            let x2 = coords[2].parse::<usize>().unwrap();
            let y2 = coords[3].parse::<usize>().unwrap();
            coords_list.push((x1, y1, x2, y2));
            if max(x1, x2) > width {
                width = max(x1, x2)
            }
            if max(y1, y2) > height {
                height = max(y1, y2)
            }
        }
    }
    // Ensuring grid size's sanity (eg. grid with only (0,0) has a size of 1, not 0)
    height += 1;
    width += 1;

    // Initiating the grid
    let mut grid = vec![0; height * width];
    for (x1, y1, x2, y2) in coords_list {
        if x1 == x2 {
            // Vertical line
            let b1 = min(y1, y2);
            let b2 = max(y1, y2);
            for y in b1..=b2 {
                grid[x1 + y * width] += 1
            }
        } else {
            // Horizontal line
            let b1 = min(x1, x2);
            let b2 = max(x1, x2);
            for x in b1..=b2 {
                grid[x + y1 * width] += 1
            }
        }
    }

    // Counting cells with a number larger than 1
    grid.iter().fold(0, |acc, n| acc + ((n > &1) as usize))
}

pub fn solve2(input: &str) -> usize {
    // List of coordinates quadruplets
    let mut coords_list = Vec::new();
    // Height and width of the grid we're going to work on
    let mut height = 0;
    let mut width = 0;
    for line in input.lines() {
        // Splintting the line and parsing numbers
        let coords = line
            .trim()
            .split(" -> ")
            .map(|pair| pair.trim().split(',').collect::<Vec<&str>>())
            .collect::<Vec<Vec<&str>>>()
            .concat();
        let x1 = coords[0].parse::<usize>().unwrap();
        let y1 = coords[1].parse::<usize>().unwrap();
        let x2 = coords[2].parse::<usize>().unwrap();
        let y2 = coords[3].parse::<usize>().unwrap();
        coords_list.push((x1, y1, x2, y2));
        if max(x1, x2) > width {
            width = max(x1, x2)
        }
        if max(y1, y2) > height {
            height = max(y1, y2)
        }
    }
    // Ensuring grid size's sanity (eg. grid with only (0,0) has a size of 1, not 0)
    height += 1;
    width += 1;

    // Initiating the grid
    let mut grid = vec![0; height * width];
    for (x1, y1, x2, y2) in coords_list {
        match (x1.cmp(&x2), y1.cmp(&y2)) {
            (Ordering::Equal, _) => {
                // Vertical line
                let b1 = min(y1, y2);
                let b2 = max(y1, y2);
                for y in b1..=b2 {
                    grid[x1 + y * width] += 1
                }
            }
            (_, Ordering::Equal) => {
                // Horizontal line
                let b1 = min(x1, x2);
                let b2 = max(x1, x2);
                for x in b1..=b2 {
                    grid[x + y1 * width] += 1
                }
            }
            (Ordering::Less, Ordering::Less) => {
                // Diagonal line, \
                let bs = x2 - x1 + 1;
                for n in 0..bs {
                    grid[(x1 + n) + (y1 + n) * width] += 1
                }
            }
            (Ordering::Less, Ordering::Greater) => {
                // Diagonal line, /
                let bs = x2 - x1 + 1;
                for n in 0..bs {
                    grid[(x1 + n) + (y1 - n) * width] += 1
                }
            }
            (Ordering::Greater, Ordering::Less) => {
                // Diagonal line, /
                let bs = x1 - x2 + 1;
                for n in 0..bs {
                    grid[(x1 - n) + (y1 + n) * width] += 1
                }
            }
            (Ordering::Greater, Ordering::Greater) => {
                // Diagonal line, \
                let bs = x1 - x2 + 1;
                for n in 0..bs {
                    grid[(x1 - n) + (y1 - n) * width] += 1
                }
            }
        }
    }

    // Counting cells with a number larger than 1
    grid.iter().fold(0, |acc, n| acc + ((n > &1) as usize))
}

#[cfg(test)]
mod tests {
    use crate::days::day05::*;

    #[test]
    fn test_day05_solve1() {
        let test_data = "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2";
        assert_eq!(solve1(test_data), 5)
    }

    #[test]
    fn test_day05_solve2() {
        let test_data = "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2";
        assert_eq!(solve2(test_data), 12)
    }
}
