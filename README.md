# Advent of code 2021

My solutions for Advent of Code's 2021 edition's puzzles, written in Rust. 🦀❤️🎄

I had two dissatisfactions with my workflow last year : first I had to manually download every day's input and put it in
an asset folder; and second my [`main`](./src/main.rs) function was full of commented-out daily-function calls. This year I solved these
issues : I use [StructOpt](https://crates.io/crates/structopt) to parse command line arguments and automatically select
the day I want, and get the input content directly from AoC's website (see [`util.rs`](./src/util.rs)).

## Use

Use cargo to build and run. Useful arguments :
- `-d <days>` to specify which days to run, with `<days>` a number from `1` to `25`, a range (`3-5`), or a list of both
  (`"5, 1-3, 9"`)
- `-f` to run each day's first puzzle's solution
- `-s` to run each day's second puzzle's solution
- `-v` to enable verbose mode (not so verbose actually)
- `-t` to enable timing of the executions

## Progression

[███████   ]

### Day 1 ⭐⭐

No problem for this first day, simply going through a list and comparing neighbors. I was able to reuse my first part's 
solution for the second part, though I'm not entirely satisfied with it (I'm convinced I could do with one `map` what 
I'm currently doing with two). Otherwise, complexity-wise, it seems good to me.

### Day 2 ⭐⭐

For this one I have really hesitated between two things : efficiency (execution speed) and code readability and reuse.
At first I wanted to create an `enum` to represent directions so that I could match on these, which would have been more
readable (and I could have reused the list making process in both parts). Then I realised doing so would mean going 
through a list two times (to make it and to use it) while I could match directly on `&str`, that is on the input lines
themselves, to implement the puzzle's logic by going only once through the list. So that's what I did.

### Day 3 ⭐⭐
Already becoming a bit challenging ! I'm not used to work on binary representation, but this was a good exercice. For 
the first part I hesitated between working on `char` and parsing binary `usize`, and thought the compiler would have 
more optimisations for the second option. Went quite well. My lack of understanding of the second puzzle made me loose 
quite some time, but in the end I'm quite satisfied with my solution. Also `clippy` made me use `.iter().enumerate().take()` 
which I don't think I've ever used before to work on `Vec` while keeping track of the index, so that's that.

[Update 04/12/2021] : Concurrency for part 2 !

### Day 4 ⭐⭐
Yay board games ! What, you say, Bingo is not a board game ? I don't care, it's a game with boards, and numbers on them.
Which means a bit of parsing and a lot of going through lists with the right indexes. And neat one-liner helper 
functions. What's not to love about today's puzzles ?

### Day 5 ⭐⭐
At first I tried solving this one using regex. It worked fine on test data but not on the real input, so I switched to a
split - parse - collect, and it did the job. Then populating a grid based on coordinates comparisons, and finishing with
a  nice `fold`. Calculating diagonals was a little tough but matching on `x.cmp(y)` did wonders, as always.

### Day 6 ⭐⭐
Exponential growth problems are so not-fun. The first part went smoothly, iterating on a growing list ... And then bam, 
second part, farewell to my naive algorithm. Thanks to a friend for suggesting the use of a map to iterate over which.

[update 06/12/2021] : Using a `Vec` instead of a `HashMap` (as indexing by integers is equivalent to using a vector).
Performance gain : approximately 10 times.

### Day 7 ⭐⭐
For this one, I followed my intuition : using median value for the first part and mean value for the second. I had 
rounding issues in the second part I still don't understand, though : on test data, rounding went fine, the expected 
answer for the actual input required me to use `floor` (which wouldn't pass the test). To bypass this, I compute both 
`floor` and `ceil` of the mean value, then take the minimum result. Tradeoff seems to be about 25% of computation time, 
that is about 100 µs on my computer, which I deem acceptable.

### Day 8 ⭐⭐
*AoC: the revenge of Map-Reduce*. Another fun day going through lists, processing values, and summing up. As a 
side-note, I tried using `i32` instead of `usize` today, as I was curious about why it was Rust's default for integers 
and read that it could be more efficient. It wasn't (or not enough, as my execution time was between 10 and 14 ms for 
both), so I switched back to `usize`.

### Day 9 ⭐⭐
No problem in finding low points for the first part. For the second, I used an approach similar to Dijkstra's algorithm,
as I had to go through a graph (a grid) from a set (a singleton) of starting points. Though this method seems efficient, 
I'm convinced it could be refined by a better use of Rust's data structures to gain milliseconds of execution time. 
Declaring a new `HashSet` in each iteration of the basin calculation (the `basin_size` function) in particular seems 
inefficient.

### Day 10 ⭐⭐
Bracket matching! I opted for a classical stack. Checking which lines are not corrupted in part 2 may not be optimal, 
but then I can omit checking if closing brackets match opening ones in incomplete lines.

### Day 11 ⭐⭐
Another grid problem, no real difficulty here. I have a lot of copy-pasted code between the two parts, I'd like to 
refactor it with a common function, but vector mutability/ownership is getting in the way. I'll figure it out when I 
have time, as it is a problem relative to the very foundation of Rust langage, and understanding and solving it would be
of great interest.

### Day 12 ⭐⭐
This was interesting. Pathfinding in graphs never fails to challenge me. Finding an appropriate algorithm was not a 
problem, as the set of paths going from start to end only once through some nodes can be represented as a tree, and thus
traversed as such. But I'm not satisfied at all with my execution times on this one (several hundreds milliseconds for 
the second part); optimisations clues may lie in graph initialisation as well as data structure choices, though I've
tried using `HashMap` instead of `Vec` to store the current path (thinking calls to `contains` would be faster) and
execution time doubled.

[update 12/12/2021] : Small performance gain (~20%) by mutating the path vector (push/pop) instead of cloning it.

[update 13/12/2021] : Another small gain (~20%) by using [Rayon](https://crates.io/crates/rayon) to parallelize computations (good ide in theory, but 
non-negligible overhead).

### Day 13 ⭐⭐
No algorithmic problem for me here, simply a matter of parsing and applying a formula based on subtracting and absolute 
value. I was quite surprised by the fact that the answer for the second part isn't a number. A nice project would be to 
parse the letters formed by the final points set, but that would require knowing what each letter is supposed to look 
like.

### Day 14 ⭐
Now that is challenging ! Today is the first day of this calendar for which I won't be able to complete the second 
puzzle on its release day. First one wasn't really hard; second one ... I can't figure it out for now, execution takes 
forever. I have tried reducing data size (I'm now mostly working on `u8` collections), storing as little data as 
possible (A `HashMap` for production rules, another one for occurrence counting) and not allocating anything in loops, 
but somehow my execution won't terminate. 

### Day 15 ⭐⭐
A* algorithm, finally ! That's the first time I've had to implement it, but I've heard of it lots of times before, and ...
It pretty much does magic. Second part wasn't too difficult, the tough point being how to deal with finitely repeating 
and modifying the initial grid. I think my solution (reasoning on a `Vec` the size of the initial grid) is pretty neat 
memory-wise (appart for the use of `usize` integers which could have been reduced to `u16` at the cost of some 
conversion time). 

### Day 16 ⭐⭐
Did I mention how much I dislike working on bits ? Pretty sure I already did. This time I had to develop a structure and
associated methods to read a stream of individual (or an arbitrary number of) bits from a hexadecimal input (thanks Rust
for iterators). I'm sure it isn't optimal and could save some calculation or useless steps, but it works.

### Day 17 ⭐⭐
No real problem today. Bruteforce worked, though I'm a bit ashamed of it. I'm sure I could (at least) refine the 
intervals I use for my loops, because at the moment the solution takes way too long (hundreds of milliseconds) to 
compute. Also, regexp.

### Day 18
Trees in Rust are hard. Trees that must know their parent are harder. I can't figure out cycling references between 
parent and children nodes, but I need it to explode nodes (or do I ?). Since I don't have a structure to work on, I 
couldn't even come up with an approaching solution today.

### Day 19
Another day with no result. I couldn't figure out how to match point sets with relative coordinates. I could find 
something with more time but I don't have it for now.

### Day 20 ⭐⭐
An interesting day, as figuring out how an infinite-sized image would behave under manipulation *wasn't trivial*. I 
confess I looked for hints on [Reddit](https://www.reddit.com/r/adventofcode/) for this one. Part 2 wasn't much of a
challenge after part 1. Also, I re-used bits of code from days 11 (neighbors-checking) and 16 (bits-vector to integer).

### Day 21 ⭐
First part was fun. Crafting a die structure was interesting, I tried making a generic structure but ultimately settled
on a `usize`-oriented implementation. Then came part 2, and its combinatory explosion. I read about memoization/dynamic
programming being the key here, but I have yet to figure out how to use it there.

### Day 22 ⭐
I swear I made part 1 work ! ... And then came part 2, where I couldn't use bruteforce (count every individual cube), so
I tried a different method (working on cuboid zones), and ... It didn't work. Lesson of the day : don't over-engineer a 
solution that is (badly) working, it may not work at all after that.

### Day 23
I will simply say I spent too much time thinking of an algorithm to solve this problem, and wasn't able to implement it
correctly.

### Day 24
Please refer to day 23.

### Day 25
The final problem doesn't seem exceptionaly complex, but I lacked time to solve it today. I may come back to it later, 
as with other ones I haven't completed yet. 

## Previous
- [2020 (Rust)](https://gitlab.com/Val.zero/advent-of-code-2020)