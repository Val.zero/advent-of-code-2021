use regex::Regex;
use std::cmp::Ordering;
use std::collections::HashSet;

pub fn solve1(input: &str) -> usize {
    let re = Regex::new(
        r"#############
#...........#
###([A-D])#([A-D])#([A-D])#([A-D])###
  #([A-D])#([A-D])#([A-D])#([A-D])#
  #########",
    )
    .unwrap();
    let caps = re.captures(input).unwrap();
    let mut cpt_a = 0;
    let mut cpt_b = 0;
    let mut cpt_c = 0;
    let mut cpt_d = 0;
    let mut amphipods = Vec::with_capacity(8);
    for i in 0..8 {
        amphipods.push(match &caps[i] {
            "A" => {
                cpt_a += 1;
                (0, cpt_a)
            }
            "B" => {
                cpt_b += 1;
                (1, cpt_b)
            }
            "C" => {
                cpt_c += 1;
                (2, cpt_c)
            }
            "D" => {
                cpt_d += 1;
                (3, cpt_d)
            }
            _ => panic!("Unknown amphipod"),
        })
    }

    // Rules for choosing which amphipod to move first
    let mut rules = HashSet::new();
    for i in 0..3 {
        // Checking amphipods in the bottoms : are they in the right column ?
        // If yes, their priority is inferior to all others
        match amphipods[i + 4] {
            (amphi, num) if amphi == i => {
                for j in 0..3 {
                    rules.insert(((amphi, num), (j, 1)));
                    rules.insert(((amphi, num), (j, 2)));
                }
            }
            _ => (),
        }

        // Amphipods at the bottom ofa column have a lesser priority than those up
        let amphi1 = amphipods[i];
        let amphi2 = amphipods[i + 4];
        rules.insert((amphi2, amphi1));
    }

    // Making the order of moving
    let mut order = amphipods.clone();
    order.sort_by(|&a, &b| {
        if rules.contains(&(a, b)) && !rules.contains(&(b, a)) {
            Ordering::Less
        } else if rules.contains(&(b, a)) && !rules.contains(&(a, b)) {
            Ordering::Greater
        } else {
            let (amphi1, num1) = a;
            let (amphi2, num2) = b;
            match amphi1.cmp(&amphi2) {
                Ordering::Equal => num1.cmp(&num2),
                Ordering::Less => Ordering::Greater,
                Ordering::Greater => Ordering::Less,
            }
        }
    });

    // let hallway = vec![None;7];
    // let rooms = amphipods.iter().map(|&x| Some(x)).collect::<Vec<_>>();
    // while !ordered(&rooms) {
    //
    // }

    0
}

pub fn solve2(_input: &str) -> usize {
    unimplemented!();
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_day23_solve1() {
        unimplemented!()
    }

    #[test]
    fn test_day23_solve2() {
        unimplemented!()
    }
}
