use std::collections::HashSet;

pub fn solve1(input: &str) -> usize {
    let grid = input
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| c.to_digit(10).unwrap() as usize)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<Vec<_>>>()
        .concat();
    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();
    let goal = grid.len() - 1;

    // We use the A* algorithm
    // Set of nodes to visit
    let mut open = HashSet::new();
    open.insert(0);
    // Distance from start
    let mut g_score = vec![usize::MAX; grid.len()];
    *g_score.get_mut(0).unwrap() = 0;
    // Heuristic function estimating distance to goal (Manhattan distance + cell cost)
    let h = |n| (n % width) + (n / width) + (grid.get(n).unwrap());
    // Estimated path length if going through each node
    let mut f_score = vec![usize::MAX; grid.len()];
    *f_score.get_mut(0).unwrap() = h(0);

    while !open.is_empty() {
        // We prioritize nodes with estimated shortest distance to goal
        let current = open.iter().fold(*open.iter().next().unwrap(), |acc, &n| {
            if f_score.get(n).unwrap() < f_score.get(acc).unwrap() {
                n
            } else {
                acc
            }
        });
        // If the point we're looking at is the goal, then we have found the shortest path
        if current == goal {
            return *g_score.get(current).unwrap();
        }
        open.remove(&current);

        // Creating the neighbors list (dealing with edge cases)
        let mut neighbors = Vec::with_capacity(4);
        if current % width != 0 {
            neighbors.push(current - 1);
        }
        if current % width != width - 1 {
            neighbors.push(current + 1);
        }
        if current / width != 0 {
            neighbors.push(current - width);
        }
        if current / width != height - 1 {
            neighbors.push(current + width);
        }

        for neighbor in neighbors {
            // We try to recalculate neighbor distance from current distance
            let tentative_g_score = g_score.get(current).unwrap() + grid.get(neighbor).unwrap();
            // It's legitimate only if the calculated distance is lesser than the previous one
            if tentative_g_score < *g_score.get(neighbor).unwrap() {
                // In this case, we save it ...
                *g_score.get_mut(neighbor).unwrap() = tentative_g_score;
                // ... re-estimate path's length if going through this node ...
                *f_score.get_mut(neighbor).unwrap() = tentative_g_score + h(neighbor);
                // ... and add it to the set of nodes to visit later
                open.insert(neighbor);
            }
        }
    }

    // This shouldn't happen, but in the case we never reach the goal, we return 0
    usize::default()
}

/// Adds `m` to `n`, eventually subtracting 9 if the result is greater than 9.
fn add_wrap(n: usize, m: usize) -> usize {
    if n + m > 9 {
        n + m - 9
    } else {
        n + m
    }
}

pub fn solve2(input: &str) -> usize {
    let grid = input
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| c.to_digit(10).unwrap() as usize)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<Vec<_>>>()
        .concat();
    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();
    // Now the input grid is only a cell in 25 times larger grid, so we calculate total values
    let total_width = width * 5;
    let total_height = height * 5;
    let total_size = grid.len() * 25;
    let goal = total_size - 1;

    // Still using the A* algorithm
    let mut open = HashSet::new();
    open.insert(0);
    let mut g_score = vec![usize::MAX; total_size];
    *g_score.get_mut(0).unwrap() = 0;
    // But now the distance from one node to another isn't just its value in the input grid, but its
    // value in the larger grid, which must be calculated
    let d = |n| {
        let abs_x = n % total_width;
        let rel_x = abs_x % width;
        let x_offset = abs_x / width;
        let abs_y = n / total_width;
        let rel_y = abs_y % height;
        let y_offset = abs_y / height;
        let val = *grid.get(rel_x + rel_y * width).unwrap();
        add_wrap(val, x_offset + y_offset)
    };
    // We only use Manhattan distance for the heuristic. It's less precise so we visit more nodes,
    // but computing the distance requires lots of small calculations which in the long run makes
    // the execution longer.
    let h = |n| (n % width) + (n / width);
    let mut f_score = vec![usize::MAX; total_size];
    *f_score.get_mut(0).unwrap() = h(0);

    // Pretty much same code from here
    while !open.is_empty() {
        let current = open.iter().fold(*open.iter().next().unwrap(), |acc, &n| {
            if f_score.get(n).unwrap() < f_score.get(acc).unwrap() {
                n
            } else {
                acc
            }
        });
        if current == goal {
            return *g_score.last().unwrap();
        }
        open.remove(&current);

        let mut neighbors = Vec::with_capacity(4);
        if current % total_width != 0 {
            neighbors.push(current - 1);
        }
        if current % total_width != total_width - 1 {
            neighbors.push(current + 1);
        }
        if current / total_width != 0 {
            neighbors.push(current - total_width);
        }
        if current / total_width != total_height - 1 {
            neighbors.push(current + total_width);
        }

        for neighbor in neighbors {
            // Note the use of d
            let tentative_g_score = g_score.get(current).unwrap() + d(neighbor);
            if tentative_g_score < *g_score.get(neighbor).unwrap() {
                *g_score.get_mut(neighbor).unwrap() = tentative_g_score;
                *f_score.get_mut(neighbor).unwrap() = tentative_g_score + h(neighbor);
                open.insert(neighbor);
            }
        }
    }

    usize::default()
}

#[cfg(test)]
mod tests {
    use crate::days::day15::*;

    #[test]
    fn test_day15_solve1() {
        let test_data = "1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581";
        assert_eq!(solve1(test_data), 40)
    }

    #[test]
    fn test_day15_solve2() {
        let test_data = "1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581";
        assert_eq!(solve2(test_data), 315)
    }
}
