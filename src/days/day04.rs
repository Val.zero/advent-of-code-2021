/// Parses a list of called numbers for the Bingo game
fn read_list(line: &str) -> Vec<usize> {
    line.trim()
        .split(',')
        .map(|n| n.parse::<usize>().unwrap())
        .collect()
}

/// Parses a board of Bingo game
fn make_board(content: &str) -> Vec<Option<usize>> {
    content
        .trim()
        .split('\n')
        .map(|line| {
            line.trim()
                .split_whitespace()
                .map(|n| Some(n.parse::<usize>().unwrap()))
                .collect::<Vec<Option<usize>>>()
        })
        .collect::<Vec<Vec<Option<usize>>>>()
        .concat()
}

/// Checks if a number is present in a Bingo board. If it is, removes it.
fn check_number(number: usize, board: &mut [Option<usize>]) {
    for cell in board {
        match cell {
            Some(n) if *n == number => *cell = None,
            _ => (),
        }
    }
}

/// Checks if a board is a winning one, that is if a whole line or column of numbers has been removed.
fn validate_board(board: &[Option<usize>]) -> bool {
    // Checking lines
    'outer1: for i in 0..5 {
        for cell in board.iter().skip(5 * i).take(5) {
            match cell {
                None => (),
                Some(_) => continue 'outer1, // There is still a number in the line, skipping to the next
            }
        }
        // No number has been found for this line, the board is a winning one
        return true;
    }

    // Checking columns the same way
    'outer2: for i in 0..5 {
        for cell in board.iter().skip(i).step_by(5) {
            match cell {
                None => (),
                Some(_) => continue 'outer2,
            }
        }
        return true;
    }

    // If no winning line or column has been found
    false
}

/// Computes the sum of remaining numbers on a board. Should be used on winning boards.
fn board_sum(board: &[Option<usize>]) -> usize {
    board.iter().fold(0, |acc, n| match n {
        None => acc,
        Some(val) => acc + val,
    })
}

pub fn solve1(input: &str) -> usize {
    // Parsing the list and Bingo boards
    let mut input_iter = input.split("\n\n");
    let num_list = read_list(input_iter.next().unwrap());
    let mut boards = Vec::new();
    for board_content in input_iter {
        boards.push(make_board(board_content))
    }

    // Checking numbers
    for n in num_list {
        for board in &mut boards {
            check_number(n, board);
            if validate_board(board) {
                return n * board_sum(board);
            }
        }
    }
    usize::default() // If no solution has been found
}

pub fn solve2(input: &str) -> usize {
    // Parsing the list and Bingo boards
    let mut input_iter = input.split("\n\n");
    let num_list = read_list(input_iter.next().unwrap());
    let mut boards = Vec::new();
    for board_content in input_iter {
        boards.push(make_board(board_content))
    }

    // List of winning boards with their last called number
    let mut winning_boards = Vec::new();
    let mut index = 0;
    // While the numbers list is not over and there are still non-winning boards
    while (index < num_list.len()) && (!boards.is_empty()) {
        let mut boards_next_state = Vec::new();
        let n = *num_list.get(index).unwrap();
        for mut board in boards {
            check_number(n, &mut board);
            if validate_board(&board) {
                // If the board is winning, we put it in the right list
                winning_boards.push((n, board));
            } else {
                // Else we want to use it in the next iteration
                boards_next_state.push(board.clone())
            }
        }
        boards = boards_next_state;
        index += 1
    }

    match winning_boards.pop() {
        None => usize::default(), // If no solution has been found
        Some((n, board)) => n * board_sum(&board),
    }
}

#[cfg(test)]
mod tests {
    use crate::days::day04::*;

    #[test]
    fn test_day04_solve1() {
        let test_data = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
";
        assert_eq!(solve1(test_data), 4512)
    }

    #[test]
    fn test_day04_solve2() {
        let test_data = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
";
        assert_eq!(solve2(test_data), 1924)
    }
}
