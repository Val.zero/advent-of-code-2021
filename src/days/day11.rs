use std::collections::HashSet;

pub fn solve1(input: &str) -> usize {
    let mut grid = input
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| c.to_digit(10).unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<Vec<_>>>()
        .concat();
    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();
    let mut flashes = 0;

    for _ in 0..100 {
        // First we increase every number in the grid by 1
        grid = grid.iter().map(|n| *n + 1).collect();

        // Then, while there are new "10" (or more) in the grid we increase the numbers around them
        let mut seen = HashSet::new();
        loop {
            let mut new10 = 0;
            for y in 0..height {
                for x in 0..width {
                    if (*grid.get(x + y * width).unwrap() >= 10) && (!seen.contains(&(x, y))) {
                        seen.insert((x, y));
                        new10 += 1;
                        if x > 0 {
                            if y > 0 {
                                grid[(x - 1) + (y - 1) * width] += 1
                            }
                            grid[(x - 1) + y * width] += 1;
                            if y < height - 1 {
                                grid[(x - 1) + (y + 1) * width] += 1
                            }
                        }
                        if y > 0 {
                            grid[x + (y - 1) * width] += 1
                        }
                        if y < height - 1 {
                            grid[x + (y + 1) * width] += 1
                        }
                        if x < width - 1 {
                            if y > 0 {
                                grid[(x + 1) + (y - 1) * width] += 1
                            }
                            grid[(x + 1) + y * width] += 1;
                            if y < height - 1 {
                                grid[(x + 1) + (y + 1) * width] += 1
                            }
                        }
                    }
                }
            }
            if new10 == 0 {
                break;
            }
        }

        // Finally, we count all "10" (or more) in the grid and rest them to 0
        grid = grid
            .iter()
            .map(|n| {
                if *n >= 10 {
                    flashes += 1;
                    0
                } else {
                    *n
                }
            })
            .collect();
    }
    flashes
}

pub fn solve2(input: &str) -> usize {
    let mut grid = input
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| c.to_digit(10).unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<Vec<_>>>()
        .concat();
    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();
    let grid_size = grid.len();
    let mut step = 0;

    loop {
        step += 1;

        // First we increase every number in the grid by 1
        grid = grid.iter().map(|n| *n + 1).collect();

        // Then, while there are new "10" (or more) in the grid we increase the numbers around them
        let mut seen = HashSet::new();
        loop {
            let mut new10 = 0;
            for y in 0..height {
                for x in 0..width {
                    if (*grid.get(x + y * width).unwrap() >= 10) && (!seen.contains(&(x, y))) {
                        seen.insert((x, y));
                        new10 += 1;
                        if x > 0 {
                            if y > 0 {
                                grid[(x - 1) + (y - 1) * width] += 1
                            }
                            grid[(x - 1) + y * width] += 1;
                            if y < height - 1 {
                                grid[(x - 1) + (y + 1) * width] += 1
                            }
                        }
                        if y > 0 {
                            grid[x + (y - 1) * width] += 1
                        }
                        if y < height - 1 {
                            grid[x + (y + 1) * width] += 1
                        }
                        if x < width - 1 {
                            if y > 0 {
                                grid[(x + 1) + (y - 1) * width] += 1
                            }
                            grid[(x + 1) + y * width] += 1;
                            if y < height - 1 {
                                grid[(x + 1) + (y + 1) * width] += 1
                            }
                        }
                    }
                }
            }
            if new10 == 0 {
                break;
            }
        }

        // Then we count all "10" (or more) in the grid and rest them to 0
        let mut zeros = 0;
        grid = grid
            .iter()
            .map(|n| {
                if *n >= 10 {
                    zeros += 1;
                    0
                } else {
                    *n
                }
            })
            .collect();

        // If all the grid is 0, we have reached the desired state
        if zeros == grid_size {
            break;
        }
    }
    step
}

#[cfg(test)]
mod tests {
    use crate::days::day11::*;

    #[test]
    fn test_day11_solve1() {
        let test_data = "5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";
        assert_eq!(solve1(test_data), 1656)
    }

    #[test]
    fn test_day11_solve2() {
        let test_data = "5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";
        assert_eq!(solve2(test_data), 195)
    }
}
