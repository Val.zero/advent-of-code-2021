fn step(floor: &mut Vec<u8>, width: usize, height: usize) -> bool {
    let mut res = false;
    // Checking which east-facing cucumbers will move
    for n in 0..floor.len() {
        let x = n % width;
        let y = n / width;
        let target = *floor.get(((x+1) % width) + y * width).unwrap();
        let c = floor.get_mut(n).unwrap();
        if c == &1 && target == 0 {
            *c = 3;
            res = true
        }
    }

    // Moving east-facing cucumbers
    for n in 0..floor.len() {
        let c = floor.get_mut(n).unwrap();
        if c == &3 {
            *c = 0;
            *floor.get_mut((n+1) % width + (n / width) * width).unwrap() = 1
        }
    }

    // Checking which south-facing cucumbers will move
    for n in 0..floor.len() {
        let x = n % width;
        let y = n / width;
        let target = *floor.get(x + ((y + 1) % height) * width).unwrap();
        let c = floor.get_mut(n).unwrap();
        if c == &2 && target == 0 {
            *c = 3;
            res = true
        }
    }

    // Moving south-facing cucumbers
    for n in 0..floor.len() {
        let c = floor.get_mut(n).unwrap();
        if c == &3 {
            *c = 0;
            *floor.get_mut(n % width + ((n+1) / width) * width).unwrap() = 2
        }
    }
    res
}

pub fn solve1(input: &str) -> usize {
    let mut floor = input.lines().map(|l|l.chars().map(|c|match c {
        '.' => 0,
        '>' => 1,
        'v' => 2,
        _ => panic!("Unrecognized character : {}", c)
    }).collect::<Vec<_>>()).collect::<Vec<_>>().concat();
    let width = input.lines().next().unwrap().len();
    let height = input.lines().count();

    let mut turn = 0;
    let mut changed = true;
    while changed {
        println!("turn {}", turn);
        for i in 0..height {
            for j in width*i..width*(i+1) {
                print!("{}", floor.get(j).unwrap())
            }
            println!()
        }
        println!();
        turn += 1;
        changed = step(&mut floor, width, height)
    }

    turn
}

pub fn solve2(_input: &str) -> usize {
    unimplemented!();
}

#[cfg(test)]
mod tests {
    use crate::days::day25::*;

    #[test]
    fn test_day25_solve1() {
        let test_data = "v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>";
        assert_eq!(solve1(test_data), 58)
    }

    #[test]
    fn test_day25_solve2() {
        unimplemented!()
    }
}
